//
//  AppDelegate.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 5/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import SwiftyStoreKit
//import PKHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        self.window = application.keyWindow ?? UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        //Switch
        if let user = Session.persistedUser, let session = Session.persisted {
            Session.current = session
            Session.currentUser = user
            self.openHomeScene()
        } else {
            self.openLoginScene()
        }
        
        self.initialAppSetup()
        
        // IAP
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        // Add any custom logic here.
        
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Helpers
    
    func showMainWindow(){
        self.window?.makeKeyAndVisible()
    }
    
    func initialAppSetup() {
        GSMessage.font = UIFont(name: "Nunito-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14)
        GSMessage.errorBackgroundColor = UIColor.templateWaning
        
        //_ = self.checkRatingReview()
        AppHandler.shared.updateAppWhenAppOpened()
    }
    
    func openLoginScene(loaderScene: Bool = true) {
        if loaderScene {
            let loaderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoaderVCID") as! LoaderVC
            self.window?.rootViewController = loaderVC
            //loaderVC.logoImageView.rotate(duration: Constants.animationTime)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.loadingTime) {
                let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVCNavID") as! UINavigationController
                self.window?.rootViewController = navController
            }
        } else {
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVCNavID") as! UINavigationController
            self.window?.rootViewController = navController
        }
    }
    
    func openHomeScene(loaderScene: Bool = true) {
        if loaderScene {
            let loaderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoaderVCID") as! LoaderVC
            self.window?.rootViewController = loaderVC
            //loaderVC.logoImageView.rotate(duration: Constants.animationTime)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.loadingTime) {
                let inititalViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNavControllerID") as! HomeNavController
                self.window?.rootViewController = inititalViewController
            }
        } else {
            let inititalViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNavControllerID") as! HomeNavController
            self.window?.rootViewController = inititalViewController
        }
    }
}

