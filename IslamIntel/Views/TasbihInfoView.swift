//
//  TasbihInfoView.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 7/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

class TasbihInfoView: UIView {
    
    // MARK: - Public
    weak var delegate: TasbihInfoViewDelegate?
    var appearanceType: AppearanceType?
    
    //Unity Side
    var alertWindow: UIWindow?
    var buttons:[String]?
    var isFullScreen = false
    
    
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var allTextView: UITextView!
    
//    @IBOutlet weak var meaningLabel: UILabel!
//    @IBOutlet weak var explanationLabel: UILabel!
//    @IBOutlet weak var creditLabel: UILabel!
    
    @IBOutlet weak var cancelOkButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!

    // MARK: - Actions
    @IBAction func cancelOkButtonTapped() {
        
        if isFullScreen {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showMainWindow()
            
            self.removeFromSuperview()
            self.alertWindow = nil
            
        } else {
            self.removeFromSuperview()
            //self.delegate?.didTapAlertViewButton(buttonType: .cancel_ok)
        }
        
        self.delegate?.didTapOkutton()
    }
    
    @IBAction func facebookButtonTapped() {
        if isFullScreen {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showMainWindow()
            self.removeFromSuperview()
            self.alertWindow = nil
        } else {
            self.removeFromSuperview()
        }
        
        self.delegate?.didTapFacebookButton()
    }

    
    // MARK: - UIView
    //for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        commonInit()
    }
    
    //for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //modalView.setc
//        modalView.layer.cornerRadius  = 8
//        modalView.layer.masksToBounds  = true
//
//        cancelOkButton.layer.cornerRadius = 6
//        cancelOkButton.layer.masksToBounds  = true
    }
    
    // MARK:  Init
    
    private func commonInit() {
        //backgroundColor = UIColor.clear
        containerView = loadNib()
        // use bounds not frame or it'll be offset
        containerView.frame = bounds
        // Adding custom subview on top of our view
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
    }
    
    func setup(frame: CGRect, tasbih: Tasbih, credit: String? = nil) {
        
        self.frame = frame
        
        modalView.setCornerRadius(16)
        cancelOkButton.setCornerRadius(8)
        
        allTextView.isEditable = false
        allTextView.isSelectable = false
        

        let titleString = "\(tasbih.transliteration!) ( \(tasbih.name!) )"
        let attributedString = NSMutableAttributedString(string: titleString)
        attributedString.addAttribute(.font, value: UIFont(name: "Nunito-Bold", size: 17)!, range: NSRange(location: 0, length: titleString.count))
        
        let nameRange = (titleString as NSString).range(of: tasbih.name!)
        attributedString.addAttribute(.font, value: UIFont(name: "Nunito-Regular", size: 17)!, range: nameRange)

        titleLabel.attributedText = attributedString

        
        //let creditText = credit ?? "\n\nCredit to:\n99 Names of Allah (Al Asma Ul Husna)"
        let allText = "\(tasbih.meaning!)\n\(tasbih.explanation!)"
        
        let allAttributedString = NSMutableAttributedString(string: allText)
        
        let meaningRange = (allText as NSString).range(of: tasbih.meaning!)
        allAttributedString.addAttribute(.font, value: UIFont(name: "Nunito-Bold", size: 14)!, range: meaningRange)
        
        let explanationRange = (allText as NSString).range(of: tasbih.explanation!)
        allAttributedString.addAttribute(.font, value: UIFont(name: "Nunito-Light", size: 14)!, range: explanationRange)
        
        //let creditRange = (allText as NSString).range(of: creditText)
        //allAttributedString.addAttribute(.font, value: UIFont(name: "Nunito-Regular", size: 14)!, range: creditRange)
        
        allTextView.attributedText = allAttributedString
        allTextView.textColor = UIColor(rgb: 0x7c7c7c, alpha: 1)


    }
    
    func showFullSceen(tasbih: Tasbih, credit: String? = nil, waitTime: Int = 0) {

        //Setup
        let frame = UIScreen.main.bounds
        self.setup(frame: frame, tasbih: tasbih, credit: credit)
        
        
        if let alertWindow = self.alertWindow {
            //without static, it'll be always nill
            alertWindow.rootViewController?.view.addSubview(self)
        } else {
            alertWindow = UIWindow(frame: UIScreen.main.bounds)
            let vc = UIViewController()
            vc.view.backgroundColor = .clear
            alertWindow?.rootViewController = vc
            alertWindow?.windowLevel = UIWindow.Level.alert + 1
            alertWindow?.makeKeyAndVisible()
            
            vc.view.addSubview(self)
        }
        
        if waitTime != 0 {
            //Hide aftr certain time
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(waitTime)) {
                //Notify Unity side
                self.removeFromSuperview()
            }
        }
        
    }
    
    
}

protocol TasbihInfoViewDelegate: class {
    func didTapOkutton()
    func didTapFacebookButton()
}

