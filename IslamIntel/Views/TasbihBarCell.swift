//
//  TasbihBarCell.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 28/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class TasbihBarCell: UITableViewCell {
    
    @IBOutlet weak var transliterationLabel: UILabel!
    @IBOutlet weak var topVerticalLine: UIView!
    @IBOutlet weak var bottomVerticalLine: UIView!
    @IBOutlet weak var barWidthLC: NSLayoutConstraint!
    
    @IBOutlet weak var horizontalBarLine: UIView!
    
    @IBOutlet weak var barLineValueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupBarWidth(value: Int, maxValue: Int) {
        
        if value == 0 {
            
            self.horizontalBarLine.isHidden = true

            self.barLineValueLabel.text = String(value)
            
            let maxwidth: CGFloat = (0.5 * self.contentView.bounds.size.width)
            self.barWidthLC.constant = -1*maxwidth
            self.contentView.layoutIfNeeded()
            
        } else {
            
            self.horizontalBarLine.isHidden = false
    
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value: value))
            
            self.barLineValueLabel.text = formattedNumber
            
            let maxwidth: CGFloat = (0.5 * self.contentView.bounds.size.width)
            
            let value: CGFloat = (1.0 - CGFloat(value)/CGFloat(maxValue))
            var constant = (value * maxwidth)
            
            if value != 0 && (maxwidth - constant) < 6 {
                constant = maxwidth - 6
            }
            
            //self.barWidthLC.constant = 0
            self.barWidthLC.constant = -1*constant
            self.contentView.layoutIfNeeded()
        }
        
        self.horizontalBarLine.setCornerRadius(2)
    }
}
