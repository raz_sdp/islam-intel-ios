//
//  HomeCell.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 7/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    
    /*
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.badgeView.setCornerRadius(12)
    }*/
    
    override func awakeFromNib() {
        //print("awakeFromNib")
        self.badgeView.setCornerRadius(12)
    }
    
}
