//
//  TasbihBarOverallCell.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 29/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class TasbihBarOverallCell: UITableViewCell {
    
    @IBOutlet weak var transliterationLabel: UILabel!
    @IBOutlet weak var topVerticalLine: UIView!
    @IBOutlet weak var bottomVerticalLine: UIView!
    
    @IBOutlet weak var overallBar: UIView!
    @IBOutlet weak var yearBar: UIView!
    @IBOutlet weak var monthBar: UIView!
    @IBOutlet weak var weekBar: UIView!
    @IBOutlet weak var dayBar: UIView!
    
    @IBOutlet weak var overallBarWidthLC: NSLayoutConstraint!
    @IBOutlet weak var yearBarWidthLC: NSLayoutConstraint!
    @IBOutlet weak var monthBarWidthLC: NSLayoutConstraint!
    @IBOutlet weak var weekBarWidthLC: NSLayoutConstraint!
    @IBOutlet weak var dayBarWidthLC: NSLayoutConstraint!
    
    @IBOutlet weak var overallValueLabel: UILabel!
    @IBOutlet weak var yearValueLabel: UILabel!
    @IBOutlet weak var monthValueLabel: UILabel!
    @IBOutlet weak var weekValueLabel: UILabel!
    @IBOutlet weak var dayValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupBarWidth(overallTasbihCount: OverallTasbihCount,  maxValue: Int) {
        self.setupBarWidth(value: overallTasbihCount.overallCount, maxValue: maxValue, bar: self.overallBar, label: self.overallValueLabel, widthLC: self.overallBarWidthLC)
        self.setupBarWidth(value: overallTasbihCount.yearCount, maxValue: maxValue, bar: self.yearBar, label: self.yearValueLabel, widthLC: self.yearBarWidthLC)
        self.setupBarWidth(value: overallTasbihCount.monthCount, maxValue: maxValue, bar: self.monthBar, label: self.monthValueLabel, widthLC: self.monthBarWidthLC)
        self.setupBarWidth(value: overallTasbihCount.weekCount, maxValue: maxValue, bar: self.weekBar, label: self.weekValueLabel, widthLC: self.weekBarWidthLC)
        self.setupBarWidth(value: overallTasbihCount.todayCount, maxValue: maxValue, bar: self.dayBar, label: self.dayValueLabel, widthLC: self.dayBarWidthLC)
    }
    
    func setupBarWidth(value: Int, maxValue: Int, bar: UIView, label: UILabel, widthLC: NSLayoutConstraint) {
        if value == 0 {
            bar.isHidden = true
            label.text = String(value)
            
            let maxwidth: CGFloat = (0.5 * self.contentView.bounds.size.width)
            widthLC.constant = -1*maxwidth
            self.contentView.layoutIfNeeded()
            
        } else {
            bar.isHidden = false
            label.isHidden = false
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value: value))
            
            label.text = formattedNumber
            
            let maxwidth: CGFloat = (0.5 * self.contentView.bounds.size.width)
            
            let value: CGFloat = (1.0 - CGFloat(value)/CGFloat(maxValue))
            var constant = (value * maxwidth)
            
            if value != 0 && (maxwidth - constant) < 6 {
                constant = maxwidth - 6
            }
            
            widthLC.constant = -1*constant
            self.contentView.layoutIfNeeded()
        }
        
        bar.setCornerRadius(2)
    }
}
