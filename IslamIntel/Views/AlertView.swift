//
//  AlertView.swift
//  GeoBit
//
//  Created by Mr. Hasan on 1/18/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

public enum AlertButtonType {
    case ok
    case yes
    case no
    case cancel
    case confirm
}

struct AlertButton {
    
    let type:AlertButtonType!
    let title:String!
    let colorHex:String!
    
    init(type: AlertButtonType, title: String, colorHex: String) {
        self.type = type
        self.title = title
        self.colorHex = colorHex
    }
}

public enum AlertViewButtonType {
    case cancel_ok
    case confirm
}

public enum AppearanceType: String {
    case none = "None"
    case confirm = "Confirm"
    case error = "Error"
    case warning = "Warning"
    case information = "Information"
    
    init(stringValue: String) {
        guard let value = AppearanceType(rawValue: stringValue) else {
            self = .none
            return
        }
        self = value
    }
}

class AlertView: UIView {
    
    // MARK: - Public
    weak var delegate: AlertViewDelegate?
    var appearanceType: AppearanceType?
    
    //Unity Side
    var alertWindow: UIWindow?
    var isUnityAlert = false
    var senderId:Int32 = 2
    var buttons:[String]?
    
    
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelOkButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelBtnTrailingToConfirmBtnLC: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnWidthLC: NSLayoutConstraint!
    
    // MARK: - Actions
    @IBAction func cancelOkButtonTapped() {
        
        self.delegate?.didTapAlertViewButton(buttonType: .cancel_ok)
        self.removeFromSuperview()
    }
    
    @IBAction func confirmButtonTapped() {
    
        self.delegate?.didTapAlertViewButton(buttonType: .confirm)
        self.removeFromSuperview()
    }
    
    // MARK: - UIView
    //for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        commonInit()
    }
    
    //for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        commonInit()
    }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        
        modalView.layer.cornerRadius  = 8
        modalView.layer.masksToBounds  = true
        cancelOkButton.layer.cornerRadius = 6
        cancelOkButton.layer.masksToBounds  = true
        confirmButton.layer.cornerRadius = 6
        confirmButton.layer.masksToBounds  = true
    }
    
    // MARK:  Init
    
    private func commonInit() {
        //backgroundColor = UIColor.clear
        containerView = loadNib()
        // use bounds not frame or it'll be offset
        containerView.frame = bounds
        // Adding custom subview on top of our view
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
    }
    
    func setup(frame: CGRect, title: String, message: String, buttons:[String], appearanceType: AppearanceType = .none) {
        
        self.frame = frame
        modalView.layer.cornerRadius  = 8
        modalView.layer.masksToBounds  = true
        cancelOkButton.layer.cornerRadius = 6
        cancelOkButton.layer.masksToBounds  = true
        confirmButton.layer.cornerRadius = 6
        confirmButton.layer.masksToBounds  = true
        
        titleLabel.text = title
        messageLabel.text = message
        
        if buttons.count == 2 {
            cancelOkButton.setTitle(buttons[0], for: .normal)
            confirmButton.setTitle(buttons[1], for: .normal)
        } else {
            cancelOkButton.setTitle(buttons[0], for: .normal)
            
            cancelOkButton.trailingAnchor.constraint(equalTo: self.modalView.trailingAnchor, constant: -20).isActive = true
            cancelBtnTrailingToConfirmBtnLC.isActive = false
            cancelBtnWidthLC.isActive = false
            confirmButton.isHidden = true
        }
    }
}

protocol AlertViewDelegate: class {
    func didTapAlertViewButton(buttonType: AlertViewButtonType)
    //@objc optional func alertViewDismiss()
}
