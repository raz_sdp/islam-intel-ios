//
//  HelpCell.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 1/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class HelpCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hLineLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
