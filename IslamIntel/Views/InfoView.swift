//
//  InfoView.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 7/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//


class InfoView: UIView {
    
    // MARK: - Public
    //weak var delegate: InfoViewDelegate?
    var appearanceType: AppearanceType?
    let CellIdentifier = "HelpCell"
    
    var help: Help!
    
    
    //Unity Side
    var alertWindow: UIWindow?
    var buttons:[String]?
    
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var helpTableView: UITableView!
    
//    @IBOutlet weak var messageOneLabel: UILabel!
//    @IBOutlet weak var midleLineLabel: UILabel!
//    @IBOutlet weak var messageTwoLabel: UILabel!
    
    @IBOutlet weak var cancelOkButton: UIButton!

    // MARK: - Actions
    @IBAction func cancelOkButtonTapped() {
        
        //self.delegate?.didTapAlertViewButton(buttonType: .cancel_ok)
        self.removeFromSuperview()
    }

    
    // MARK: - UIView
    //for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        commonInit()
    }
    
    //for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        //modalView.setc
//        modalView.layer.cornerRadius  = 8
//        modalView.layer.masksToBounds  = true
//
//        cancelOkButton.layer.cornerRadius = 6
//        cancelOkButton.layer.masksToBounds  = true
    }
    
    // MARK:  Init
    
    private func commonInit() {
        //backgroundColor = UIColor.clear
        containerView = loadNib()
        // use bounds not frame or it'll be offset
        containerView.frame = bounds
        // Adding custom subview on top of our view
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        
        //let cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        //helpTableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        //helpTableView.register(HelpCell.self, forCellReuseIdentifier: CellIdentifier)
        helpTableView.register(UINib(nibName: "HelpCell", bundle: nil), forCellReuseIdentifier: CellIdentifier)
        
        helpTableView.rowHeight = UITableView.automaticDimension
        helpTableView.estimatedRowHeight = 44
    }
    
    func setup(frame: CGRect, title: String, message: String, button:String, appearanceType: AppearanceType = .none) {
        
        self.frame = frame
        
        modalView.setCornerRadius(16)
        cancelOkButton.setCornerRadius(8)
        
        helpTableView.delegate = self
        helpTableView.dataSource = self
        
        /*
        titleLabel.text = title
        messageOneLabel.text = message
        messageTwoLabel.text = message
        cancelOkButton.setTitle(button, for: .normal)
        */
    }
    
    func setup(frame: CGRect, help: Help) {
        
        self.frame = frame
        
        modalView.setCornerRadius(16)
        cancelOkButton.setCornerRadius(8)
        
        self.help = help
        
        helpTableView.delegate = self
        helpTableView.dataSource = self
        
        helpTableView.reloadData()
        /*
         titleLabel.text = title
         messageOneLabel.text = message
         messageTwoLabel.text = message
         cancelOkButton.setTitle(button, for: .normal)
         */
    }
}

extension InfoView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return help.description.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
//        cell.textLabel?.font = UIFont(name: "Nunito-Light", size: 16)!
//        cell.textLabel?.text = help.description[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! HelpCell
        cell.descriptionLabel?.text = help.description[indexPath.row]
        
        if indexPath.row == help.description.count - 1 {
            cell.hLineLabel.isHidden = true
        } else {
            cell.hLineLabel.isHidden = false
        }
        
        return cell
    }
}

protocol InfoViewDelegate: class {
    func didTapAlertViewButton(buttonType: AlertViewButtonType)
    //@objc optional func alertViewDismiss()
}

//class HelpCell: UITableViewCell {
//
//    
//
//    //for using in code
////    override init(frame: CGRect) {
////        super.init(frame: frame)
////
////        print("HelpCell: init(frame: CGRect)")
////    }
//    
//    //for using in IB
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        
//        print("HelpCell: init?(coder aDecoder: NSCoder)")
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//        print("HelpCell: func awakeFromNib()")
//
//    }
//    
//}

