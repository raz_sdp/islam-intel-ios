//
//  PurchaseCell.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 16/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import StoreKit

class PurchaseCell: UITableViewCell {
    
    @IBOutlet weak var purchaseBtn: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    var cellPurchaseBtnTapped: (() -> Void)?
    
    static let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        
        return formatter
    }()
    
    var product: SKProduct? {
        didSet {
            guard let product = product else { return }
            
            infoLabel?.text = product.localizedTitle
            
            
            //if IslamIntelProducts.store.isProductPurchased(product.productIdentifier) {
            
            let boolValue = UserDefaults.standard.bool(forKey: product.productIdentifier)
            let dateValue = UserDefaults.standard.value(forKey: product.productIdentifier + "expiryDate") as? Date

            if boolValue {
                
                purchaseBtn.setTitle("Subscribed to Tasbih Tracker Premium", for: .normal)
                purchaseBtn.isEnabled = false
                
                //accessoryType = .checkmark
                //accessoryView = nil
                //detailTextLabel?.text = ""
                
                PurchaseCell.priceFormatter.locale = product.priceLocale
                let priceText = PurchaseCell.priceFormatter.string(from: product.price) ?? product.localizedPrice!
                infoLabel.text = "You have access to all Tasbih Tracker features at a cost of \(priceText) annually. Each renewal will be charged from your iTunes account. Subscriptions can be cancelled at any time from your iTunes account."
                
            } else if SKPaymentQueue.canMakePayments() {
                
                //accessoryType = .none
                //accessoryView = self.newBuyButton()
                    
                purchaseBtn.setTitle("Subscribe to Tasbih Tracker Premium", for: .normal)
                purchaseBtn.isEnabled = true
                
                PurchaseCell.priceFormatter.locale = product.priceLocale
                let priceText = PurchaseCell.priceFormatter.string(from: product.price) ?? product.localizedPrice!
                //"Get access to all Tasbih Tracker features at a cost of £4.99 annually. Each renewal will be charged from your iTunes account. Subscriptions can be cancelled at any time from your iTunes account."
                infoLabel.text = "Get access to all Tasbih Tracker features at a cost of \(priceText) annually. Each renewal will be charged from your iTunes account. Subscriptions can be cancelled at any time from your iTunes account."
                
            } else {
                purchaseBtn.setTitle("Subscribe to Tasbih Tracker Premium", for: .normal)
                purchaseBtn.isEnabled = false
                infoLabel.text = "Not available! Unknown error. Please contact support"
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        purchaseBtn.setTitle("", for: .normal)
        purchaseBtn.isEnabled = true
        infoLabel?.text = ""
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func purchaseBtnTapped(_ sender: Any) {
        self.cellPurchaseBtnTapped?()
    }
}
