//
//  Constants.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 5/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation

public enum TimeType: String {
    case today = "day"
    case week = "week"
    case month = "month"
    case year = "year"
    case overall = "overall"

    init(stringValue: String) {
        guard let value = TimeType(rawValue: stringValue) else {
            self = .today
            return
        }
        self = value
    }
}


struct Constants {
    
    static let loadingTime = 3.0
    static let animationTime = 3.0
    static let displayTime = 3.0
    static let flashTime = 1.5
    
    static var maxTasbihCountLimit = 10000
    
    static let buildName = "IslamIntel"
    static let appName = "Islam Intel"
    static let appId = "id1409442221"
    static let appURL = "https://itunes.apple.com/us/app/islam-intel/id1409442221?ls=1&mt=8"
    static let purchaseAppBundleId = "com.islamintel.IslamIntel"
    
    static let developmentBaseURL = "http://www.islamintel.com/"
    static let productionBaseURL = "http://www.islamintel.com/"
    
    static let feedbackEmail = "hr@xorbd.com"
    
    struct FacebookConstants {
        static let clientId = "asdasdsa"
    }
    
    struct API {
        static let Login = "users/login"
        static let SignUp = "users/signup"
        static let EditProfile = "users/account_update"
        static let ResetPassword = "users/reset_password"
        static let ValidateInput = "users/validate_input"
        static let GetHelp = "helps/get_help"
        
        static let Feedback = "feedback/submit_feedback"
        
        //Tasbih
        static let GetTasbihList = "tasbihoptions/get_tasbih_list"
        static let AddTasbihCount = "Tasbihhistories/add_tasbih_count"
        static func GetCountTasbih(userId: String, timeType: TimeType = .today) -> String {
            return "Tasbihhistories/get_count_tasbih/\(userId)/\(timeType.rawValue)"
        }
        static func GetEditTasbihHistory(userId: String, timeType: TimeType = .today) -> String {
            return "Tasbihhistories/edit_tasbih_histories/\(userId)/\(timeType.rawValue)"
        }
        static let DeleteTasbihHistory = "Tasbihhistories/delete_tasbih"
    }
    
    struct IAP {
        static let SharedSecret = "402e0cc3fcb84abfa7dc0528c14492c5"
        
        static let TasbihTrackerYearly = "com.islamintel.IslamIntel.TasbihTrackerYearly"
    }
    
    struct Tasbih {
        static let loadingTime = 3.0
    }
}

