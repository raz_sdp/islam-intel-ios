//
//  Request.swift
//
//  Created by Mehedi Hasan on 2/28/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper

let HTTPURLResponseKey = "HTTPURLResponseKey"

class Request {
    
    // MARK: - Constants
    #if DEBUG
    static let baseUrlString = Constants.developmentBaseURL
    #else
    static let baseUrlString = Constants.productionBaseURL
    #endif
    fileprivate static let baseUrl = URL(string: baseUrlString)
    
    // MARK: - Public
    @discardableResult static func send<T: BaseMappable>(_ method: Alamofire.HTTPMethod, path: String, parameters: [String: AnyObject] = [String: AnyObject](), encoding: ParameterEncoding = JSONEncoding.default, response done: ((_ response: T?, _ error: NSError?) -> ())? = nil) -> Request {
        
        let mapClosure = { Mapper<T>().map(JSONObject: $0) }
        return sendReq(mapClosure, method: method, path: path, parameters: parameters, encoding: encoding, response: done)
    }
    
    @discardableResult static func send<T: BaseMappable>(_ method: Alamofire.HTTPMethod, path: String, parameters: [String: AnyObject] = [String: AnyObject](), encoding: ParameterEncoding = JSONEncoding.default, response done: ((_ response: [T]?, _ error: NSError?) -> ())? = nil) -> Request {
        
        let mapClosure = { Mapper<T>().mapArray(JSONObject: $0) }
        return sendReq(mapClosure, method: method, path: path, parameters: parameters, encoding: encoding, response: done)
    }
    
    @discardableResult static func send(_ method: Alamofire.HTTPMethod, path: String, parameters: [String: AnyObject] = [String: AnyObject](), encoding: ParameterEncoding = JSONEncoding.default, response done: ((_ error: NSError?) -> ())? = nil) -> Request {
        return send(method, path: path, parameters: parameters, encoding:encoding) { (_: Response?, error: NSError?) in
            if let done = done {
                done(error)
            }
        }
    }
    
    func abort() {
        dispatchGroup.notify(queue: DispatchQueue.main) {
            self.dataRequest?.cancel()
        }
    }
    
    // MARK: Private
    fileprivate static func sendReq<T>(_ map: @escaping (Any) -> T?, method: Alamofire.HTTPMethod, path: String, parameters: [String: AnyObject] = [String: AnyObject](), encoding: ParameterEncoding, response done: ((_ response: T?, _ error: NSError?) -> ())? = nil) -> Request {
        
        guard let url = URL(string: path, relativeTo: baseUrl) else {
            fatalError()
        }
        
        let headers = HTTPHeaders()
        
        /*
         if let tokenId = Session.current?.tokenId {
            headers["X-XORBD-SESSIONID"] = tokenId
        }*/
        
        print("url:: \(url) \(url) Param:\(parameters)")
        
        let req = self.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        req.responseJSON { response in
            
            print("\(path) | \(String(describing: response.response?.statusCode)) | \(response)")
            
            var result: T? = nil
            var error: NSError? = nil
            
            switch response.result {
            case .success(let json):
                guard let code = response.response?.statusCode, (200..<300).contains(code) else {
                    let errorMessage = "Error"
                    /*
                    if let error = Mapper<Response.Error>().map(JSONObject: json), let message = error.message {
                        errorMessage = message
                    }*/
                    
                    error = networkError(errorMessage, response: response)
                    break
                }
                
                if let parsed = map(json) {
                    result = parsed
                }
                
            case .failure(let serverError):
                guard !(response.response?.statusCode != nil &&
                    (200..<300).contains(response.response!.statusCode) &&
                    response.data?.count == 0) else {
                        break
                }
                
                error = networkError(serverError.localizedDescription , response: response, error: serverError as NSError?)
            }
            
            if let done = done {
                done(result, error)
            }
        }
        
        return req
    }
    
    
    static func request(_ url: URL, method: Alamofire.HTTPMethod, parameters: [String: AnyObject], encoding: ParameterEncoding,  headers: HTTPHeaders) -> Request {
        let request = Request()
        
        if method != .get && parameters.values.contains(where: { $0 is UIImage || $0 is URL }) {
            request.dispatchGroup.enter()
            DispatchQueue.global(qos: .userInitiated).async {
                
                Alamofire.upload(
                    multipartFormData: { multipartFormData in
                        
                        //multipartFormData.append(unicornImageURL, withName: "unicorn")
                        for (key, value) in parameters {
                            
                            if let image = value as? UIImage {
                                //Compress image here
                                if let data = image.jpegData(compressionQuality: 0.7) {
                                    multipartFormData.append(data, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
                                }
                            } else {
                                if let data = "\(value)".data(using: String.Encoding.utf8) {
                                    multipartFormData.append(data, withName: key)
                                }
                            }
                        }
                },
                    to: url,
                    method: method,
                    headers: headers,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            request.dataRequest = upload
                            
                            /*upload.uploadProgress(closure: { progress in
                                print("current progress \(progress.fractionCompleted)")
                            })
                            upload.responseJSON { response in
                             print(response)
                             }*/
                            
                            request.dispatchGroup.leave()
                            
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                }
                )
                /*
                 DispatchQueue.main.async {
                 // Bounce back to the main thread to update the UI
                 }*/
            }
        } else {
            request.dataRequest = Alamofire.request(url, method: method, parameters: parameters, encoding:encoding, headers: headers)
        }
        
        return request
    }
    
    static func networkError(_ message: String, response: Alamofire.DataResponse<Any>, error: NSError? = nil) -> NSError {
        
        var info: [String: AnyObject] = [NSLocalizedDescriptionKey: message as AnyObject, HTTPURLResponseKey: response.response ?? NSNull()]
        
        if let error = error {
            
            info[NSUnderlyingErrorKey] = error
        }
        
        return NSError(domain: NSStringFromClass(self), code: 0, userInfo: info)
    }
    
    fileprivate var dispatchGroup = DispatchGroup()
    fileprivate var dataRequest: Alamofire.DataRequest?
    
    func responseJSON(_ completionHandler: @escaping (Alamofire.DataResponse<Any>) -> Void) {
        
        dispatchGroup.notify(queue: DispatchQueue.main) {
            
            self.dataRequest?.responseJSON(completionHandler: completionHandler)
        }
    }
    
    // MARK: - Helpers
    
    static func imageURL(_ imageID: String?) -> URL {
        guard let imageID = imageID, imageID != "" else {
            return URL(fileURLWithPath: Bundle.main.path(forResource: "noImage", ofType: "png")!)
        }
        
        let ulrString = "\(baseUrlString)pictures/\(imageID)"
        guard let url = URL(string: ulrString) else {
            fatalError()
        }
        
        return url
    }
    
}
