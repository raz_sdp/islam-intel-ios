//
//  Transform.swift
//  RemedyMessenger
//
//  Created by Mehedi Hasan on 2/28/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper

class Transform {
    
    // MARK: - Public
    
    static let toString = TransformOf<String, Int>(
        fromJSON: { String($0!) },
        toJSON: { $0.map { Int($0)! }}
    )
    
    static let toBool = TransformOf<Bool, String>(
        fromJSON: { $0?.lowercased() == "yes" ? true : false },
        toJSON: { $0 == true ? "yes" : "no" }
    )
    
    static let intToBool = TransformOf<Bool, Int>(
        fromJSON: { $0 == 1 ? true : false },
        toJSON: { $0 == true ? 1 : 0 }
    )
    
    static func toEnum<E: RawRepresentable>() -> TransformOf<E, String> where E.RawValue == String {
        return TransformOf<E, String>(
            fromJSON: {
                if let value = $0 {
                    return E(rawValue: value.lowercased())
                }
                return nil
        },
            toJSON: {
                if let value = $0 {
                    return value.rawValue
                }
                return nil
        })
    }
    
    static func intToEnum<E: RawRepresentable>() -> TransformOf<E, Int> where E.RawValue == Int {
        return TransformOf<E, Int>(
            fromJSON: {
                if let value = $0 {
                    return E(rawValue: value)
                }
                return nil
        },
            toJSON: {
                if let value = $0 {
                    return value.rawValue
                }
                return nil
        })
    }
    
    static let toDate = TransformOf<Date, String>(
        fromJSON: {
            if let value = $0 {
                return dateFormatter.date(from: value)
            }
            return nil
    },
        toJSON: {
            if let value = $0 {
                return dateFormatter.string(from: value)
            }
            return nil
    }
    )
    
    static let toTime = TransformOf<Date, String>(
        fromJSON: {
            if let value = $0 {
                return timeFormatter.date(from: value)
            }
            return nil
    },
        toJSON: {
            if let value = $0 {
                return timeFormatter.string(from: value)
            }
            return nil
    }
    )
    
    static let toDateTime = TransformOf<Date, String>(
        fromJSON: {
            if let value = $0 {
                return dateTimeFormatter.date(from: value)
            }
            return nil
    },
        toJSON: {
            if let value = $0 {
                return dateTimeFormatter.string(from: value)
            }
            return nil
    }
    )
    
    static let toEncodeString = TransformOf<String, String>(
        fromJSON: {
            if let value = $0 {
                let data = value.data(using: String.Encoding.utf8)
                return String(data: data!, encoding: String.Encoding.nonLossyASCII) ?? value
            }
            return nil
    },
        toJSON: {
            if let value = $0 {
                let data = value.data(using: String.Encoding.nonLossyASCII)
                return String(data: data!, encoding: String.Encoding.utf8) ?? value
            }
            return nil
    }
    )
    
    // MARK: - Private
    
    fileprivate static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        //formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.dateFormat = "yyyy-MM-dd"
        
        return formatter
    }()
    
    fileprivate static var timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        //formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.dateFormat = "HH:mm:ss"
        
        return formatter
    }()
    
    fileprivate static var dateTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        //formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return formatter
    }()
    
}
