//
//  TasbihCount.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 28/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper

class TasbihCount: Response {
    
    var transliteration_id: String!
    var transliteration: String!
    var count: Int!
    
    override func mapping(map: Map) {
        super.mapping(map: map)

        transliteration_id <- map["transliteration_id"]
        transliteration <- map["transliteration"]
        count <- map["count"]
    }
}

class TasbihCountList: Response {
    var success: Bool!
    var countList: [TasbihCount]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        success <- map["success"]
        countList <- map["data"]
    }
}

class OverallTasbihCount: Response {
    
    var transliteration_id: String!
    var transliteration: String!
    var todayCount: Int!
    var weekCount: Int!
    var monthCount: Int!
    var yearCount: Int!
    var overallCount: Int!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        transliteration_id <- map["transliteration_id"]
        transliteration <- map["transliteration"]
        todayCount <- map["count.today"]
        weekCount <- map["count.week"]
        monthCount <- map["count.month"]
        yearCount <- map["count.year"]
        overallCount <- map["count.overall"]
    }
}

class OverallTasbihCountList: Response {
    var success: Bool!
    var countList: [OverallTasbihCount]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        success <- map["success"]
        countList <- map["data"]
    }
}

// Edit History Count

class TasbihHistory: Response {
    
    var id: String!
    var createdDate: Date!
    var count: Int!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        createdDate <- (map["created"], Transform.toDateTime)
        count <- map["count"]
    }
}

class TasbihHistoryOption: Response {
    var optionName: String!
    var tasbihHistoryList: [TasbihHistory]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        optionName <- map["option_name"]
        tasbihHistoryList <- map["data"]
    }
}

class EditTasbihHistory: Response {
    var success: Bool!
    var tasbihHistoryOptionList: [TasbihHistoryOption]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        success <- map["success"]
        tasbihHistoryOptionList <- map["data"]
    }
}
