//
//  Session.swift
//  Session
//
//  Created by Mr. Hasan on 3/1/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import Foundation


struct FacebookUser {
    let email: String
    let pass: String
    
    init(email: String, pass: String) {
        self.email = email
        self.pass = pass
    }
}

class Session {
    
    //MARK:-  Constant
    static let CurrentUserChangedNotification = "Session.Notifications.CurrentUserChanged"
    
    fileprivate enum Key: String {
        case TokenId = "auth-token"
        case UserId = "auth-userId"
        case UserPass = "auth-userPass"
        case UserEmail = "auth-userEmail"
        
        //case JWToken = "auth-jwToken"
        case FBEmail = "auth-FBEmail"
        case FBPass = "auth-FBPass"
    }
    
    fileprivate enum UserKey: String {
        case UserId = "ssn-UserId"
        case UserName = "ssn-UserName"
        case UserGender = "ssn-UserGender"
        case userDOB = "ssn-userDOB"
        case UserEmail = "ssn-UserEmail"
        
        case UserFBId = "ssn-UserFBId"
    }
    
    //MARK:-  Public
    
    public static var currentUser: User? {
        willSet {
            //persist currentUser
        }
        didSet{
            let defaults = UserDefaults.standard
            defaults.set(currentUser?.userId, forKey: UserKey.UserId.rawValue)
            defaults.set(currentUser?.name, forKey: UserKey.UserName.rawValue)
            defaults.set(currentUser?.gender, forKey: UserKey.UserGender.rawValue)
            defaults.set(currentUser?.birthDate.toString(format: .isoDate), forKey: UserKey.userDOB.rawValue)
            defaults.set(currentUser?.email, forKey: UserKey.UserEmail.rawValue)
            defaults.synchronize()
        }
    }
    
    public static var persistedUser: User? {
        get {
            let defaults = UserDefaults.standard
            
            guard let userId = defaults.string(forKey:  UserKey.UserId.rawValue) else {
                return nil
            }
            guard let name = defaults.string(forKey: UserKey.UserName.rawValue) else {
                return nil
            }
            guard let dobStr = defaults.string(forKey: UserKey.userDOB.rawValue) else {
                return nil
            }
            guard let gender = defaults.string(forKey: UserKey.UserGender.rawValue) else {
                return nil
            }
            guard let email = defaults.string(forKey: UserKey.UserEmail.rawValue) else {
                return nil
            }
            guard let birthDate = Date(fromString: dobStr, format: .isoDate) else {
                return nil
            }
            
            let user = User(userId: userId, name: name, birthDate: birthDate, gender: gender, email: email)
            
            return user
        }
    }
    
    public static var currentFBUser: FacebookUser? {
        willSet {
            //persist currentUser
        }
        didSet{
            let defaults = UserDefaults.standard
            defaults.set(currentFBUser?.email, forKey: Key.FBEmail.rawValue)
            defaults.set(currentFBUser?.pass, forKey:  Key.FBPass.rawValue)
            defaults.synchronize()
        }
    }
    
    public static var persistedFacebookUser: FacebookUser? {
        get {
            let defaults = UserDefaults.standard
            guard let email = defaults.string(forKey: Key.FBEmail.rawValue) else {
                return nil
            }
            guard let pass = defaults.string(forKey: Key.FBPass.rawValue) else {
                return nil
            }

            let fbUser = FacebookUser(email: email, pass: pass)
            return fbUser
        }
    }

    
    
    
    static var current: Session? {
        willSet {
            current?.persist()
        }
        didSet{
            let defaults = UserDefaults.standard
            defaults.set(current?.tokenId, forKey: Key.TokenId.rawValue)
            defaults.set(current?.userId, forKey: Key.UserId.rawValue)
            defaults.set(current?.userPass, forKey: Key.UserPass.rawValue)
            defaults.set(current?.userEmail, forKey: Key.UserEmail.rawValue)
            defaults.synchronize()
            current?.persist()
        }
    }
    
    static var persisted: Session? {
        get {
            let defaults = UserDefaults.standard
            guard let token = defaults.string(forKey: Key.TokenId.rawValue) else {
                return nil
            }
            guard let userId = defaults.string(forKey: Key.UserId.rawValue) else {
                return nil
            }
            guard let pass = defaults.string(forKey: Key.UserPass.rawValue) else {
                return nil
            }
            guard let email = defaults.string(forKey: Key.UserEmail.rawValue) else {
                return nil
            }
            
            return Session(tokenId: token, userId: userId, password: pass, email: email)
        }
    }
    
    init(tokenId: String, userId: String, password: String, email: String) {
        self.tokenId = tokenId
        self.userId = userId
        self.userPass = password
        self.userEmail = email
    }
    
    deinit {
        //Remove any Notification
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CurrentUserChangedNotification), object: nil)
    }
    
    // MARK: - Private
    
    fileprivate(set) var tokenId: String
    fileprivate(set) var userId: String
    fileprivate(set) var userPass: String
    fileprivate(set) var userEmail: String
    
    fileprivate func persist() {
        let defaults = UserDefaults.standard
        // Store any other data
        
        defaults.synchronize()
    }
    
//    fileprivate static func postNotification(_ name: String, error: NSError? = nil, message: String? = nil) {
//        var userInfo = [String: AnyObject]()
//        if let error = error {
//            userInfo[NSUnderlyingErrorKey] = error
//        }
//        userInfo[NSLocalizedDescriptionKey] = (message ?? error?.localizedDescription) as AnyObject
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: name), object: self, userInfo: userInfo)
//    }
    
}
