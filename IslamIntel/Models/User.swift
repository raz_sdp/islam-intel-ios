//
//  User.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 8/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper

enum Gender: String {
    case male = "male"
    case female = "female"
    
    init(stringValue: String) {
        guard let value = Gender(rawValue: stringValue) else {
            self = .male
            return
        }
        self = value
    }
}

class User: Response {
    
    var userId: String!
    var name: String!
    var birthDate: Date!
    var gender: String!
    var email: String!
    
    //var profileImage: String!
    //var personalInfo: String!

    var facebookId: String?
    //var password: String?
    
    convenience init(userId: String, name: String, birthDate: Date, gender:String, email: String) {
        self.init()
        self.userId = userId
        self.name = name
        self.birthDate = birthDate
        self.gender = gender
        self.email = email
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["id"]
        name <- map["fullname"]
        birthDate <- (map["dob"], Transform.toDate)
        gender <- map["gender"]
        email <- map["email"]
    }
}
