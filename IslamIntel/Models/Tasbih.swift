//
//  Tasbih.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 23/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper

//enum Gender: String {
//    case male = "male"
//    case female = "female"
//
//    init(stringValue: String) {
//        guard let value = Gender(rawValue: stringValue) else {
//            self = .male
//            return
//        }
//        self = value
//    }
//}

class Tasbih: Response {
    
    var id: String!
    var name: String!
    var transliteration: String!
    var meaning: String!
    var explanation: String!
    var image: String?
    
    /*convenience init(userId: String, name: String, birthDate: Date, gender:String, email: String) {
        self.init()
        
        self.userId = userId
        self.name = name
        self.birthDate = birthDate
        self.gender = gender
        self.email = email
    }*/
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        transliteration <- map["transliteration"]
        meaning <- map["meaning"]
        explanation <- map["explanation"]
        image <- map["image"]
    }
}

class TasbihList: Response {
    
    var tasbihList: [Tasbih]!

    override func mapping(map: Map) {
        super.mapping(map: map)
        
        tasbihList <- map["listData"]
    }
}

