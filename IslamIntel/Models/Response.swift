//
//  Response.swift
//  Response
//
//  Created by Mehedi Hasan on 2/28/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper


class Response: Mappable {
    
    required init?(map: Map) {
    }
    
    init(){
    }
    
    func mapping(map: Map) {
    }
}

extension Response {
    
    /*
    class Error: Response {
        var title: String!
        var message: String!
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            
            title <- map["error.title"]
            message <- map["error.message"]
        }
    }
    
    class Success: Response {
        var title: String!
        var message: String!
        var data: [String : Any]?
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            
            title <- map["success.title"]
            message <- map["success.message"]
            data <- map["success.data"]
        }
    }*/
    
    class SuccessResponse: Response {
        var success: Bool!
        override func mapping(map: Map) {
            super.mapping(map: map)
            success <- map["success"]
        }
    }
    
    class BaseResponse: Response {
        var success: Bool!
        var message: String?
    
        override func mapping(map: Map) {
            super.mapping(map: map)
            
            success <- map["success"]
            message <- map["msg"]
        }
    }
    
    class LoginResponse: BaseResponse {
        var user: User?
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            user <- map["userdata"]
        }
    }

    class SignUpResponse: BaseResponse {
        //var user: User?
        //It'll crash as API doesn't return user ID
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            //user <- map["userdata"]
        }
    }
    
    /*
    class ValidationResponse: BaseResponse {
        //var user: User?
        //It'll crash as API doesn't return user ID
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            //user <- map["userdata"]
        }
    }*/

}




