//
//  HelpText.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 1/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import ObjectMapper

class HelpText: Response {
    
    var helpTextList: [Help]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        helpTextList <- map["data"]
    }
    
    func getHelpText(pageTitle: String) -> Help? {
        guard let helpTextList = helpTextList else {
            return nil
        }
        for help in helpTextList {
            if help.pageTitle == pageTitle {
                return help
            }
        }
        
        return nil
    }
}

class Help: Response {
    
    var moduleName: String!
    var pageTitle: String!
    var description: [String]!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        moduleName <- map["module_name"]
        pageTitle <- map["page_title"]
        description <- map["description"]
    }
}
