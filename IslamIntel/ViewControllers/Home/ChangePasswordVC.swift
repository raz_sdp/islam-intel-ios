//
//  ChangePasswordVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class ChangePasswordVC: UIViewController, UITextFieldDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var oldPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmNewPasswordTF: UITextField!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    //@IBOutlet weak var helpBtn: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var oldPassLabel: UILabel!
    @IBOutlet weak var newPassLabel: UILabel!
    @IBOutlet weak var confirmPassLabel: UILabel!
    
    //MARK:- Property
    fileprivate weak var currentTF: UITextField?
    //let pageTitle = "Sign Up"
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScene()
        
        //self.navigationController?.navigationBar.isHidden = true
        
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //self.nameTF.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func backBtnTapped(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*@IBAction func helpBtnTapped(_ sender: Any){
     if AppHandler.shared.helpText == nil {
     self.loadHelpText()
     return
     }
     
     guard let help = AppHandler.shared.helpText!.getHelpText(pageTitle: self.pageTitle) else {
     return
     }
     
     self.view.endEditing(true)
     
     let infoView = InfoView()
     infoView.setup(frame: self.view.bounds, help: help)
     self.view.addSubview(infoView)
     return
     }*/
    
    @IBAction func saveBtnTapped(_ sender: Any){
        
        guard let oldPassword = oldPasswordTF.text, oldPassword == Session.current!.userPass else {
            self.oldPasswordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("please enter old password correctly", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.oldPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        guard let newPassword = newPasswordTF.text, newPassword.count > 3 else {
            self.newPasswordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("4-12 charactes allowed", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.newPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        guard newPassword.count == newPassword.trim().count else {
            self.newPasswordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("white spaces aren't allowed", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.newPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        guard let confirmNewPassword = confirmNewPasswordTF.text, newPassword == confirmNewPassword else {
            self.confirmNewPasswordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("password doesn't match", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.confirmNewPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        /// API Call
        
        let parameters = [
            "id":  Session.currentUser!.userId,
            "password": newPassword
        ]
        
        let formattedParameters: [String : AnyObject] = [
            "User": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.EditProfile, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.LoginResponse?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard response.success else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage((response.message ?? "Changing password failed"), type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard let user = response.user else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage("Something went wrong!", type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            Session.currentUser = user
            Session.current = Session(tokenId: "", userId: "", password: newPassword, email: user.email)
            
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Password is changed successfully"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                self.backBtnTapped(self)
            }
        }
    }
    
    //MARK: Helpers
    
    
    
    func setupScene() {
        //self.helpBtn.image = UIImage(named:"help")?.withRenderingMode(.alwaysOriginal)
        
        self.oldPasswordTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.newPasswordTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.confirmNewPasswordTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        
        self.oldPasswordTF.setLeftPadding(width: 12)
        self.newPasswordTF.setLeftPadding(width: 12)
        self.confirmNewPasswordTF.setLeftPadding(width: 12)
        
        self.saveBtn.setCornerRadius(4)
        
        fontSetup(oldPassLabel)
        fontSetup(newPassLabel)
        fontSetup(confirmPassLabel)
    }
    
    func fontSetup(_ label: UILabel){
        let attributedString = NSMutableAttributedString(string: label.text!)
        
        let attributes0: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor.templateGray,
            .font: UIFont(name: "Nunito-Light", size: 16)!
        ]
        attributedString.addAttributes(attributes0, range: NSRange(location: 0, length: (label.text!.count-1)))
        
        let attributes1: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor.templateBase
        ]
        attributedString.addAttributes(attributes1, range: NSRange(location: (label.text!.count-1), length: 1))
        
        label.attributedText = attributedString
    }
    
    func loadHelpText(){
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("ShouldBeginEditing: \(textField.tag)")
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("DidBeginEditing: \(textField.tag)")
        textField.layer.borderColor = UIColor.templateBase.cgColor
        self.currentTF = textField
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("ShouldEndEditing: \(textField.tag)")
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing: \(textField.tag)")
        textField.layer.borderColor = UIColor.borderGray.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if textField == self.oldPasswordTF {
            self.newPasswordTF.becomeFirstResponder()
        } else if textField == self.newPasswordTF {
            self.confirmNewPasswordTF.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        return newLength <= 12
    }
}
