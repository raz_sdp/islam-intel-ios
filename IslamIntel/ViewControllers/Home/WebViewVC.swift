//
//  WebViewVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 13/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import WebKit
import PKHUD

enum SourceType {
    case aboutUs
    case privacyPolicy
}

class WebViewVC: UIViewController {
    
    var sourceType: SourceType = .aboutUs
    
    let fileNames: [SourceType: String] = [
        .aboutUs: "AboutUs",
        .privacyPolicy: "PrivacyPolicy"
    ]
    
    let titles: [SourceType: String] = [
        .aboutUs: "About Us",
        .privacyPolicy: "Privacy Policy"
    ]
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    // instance of WKWebView
    let wkWebView: WKWebView = {
        let v = WKWebView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = titles[sourceType]
        wkWebView.backgroundColor = .white
        wkWebView.navigationDelegate = self
        self.view.addSubview(wkWebView)
        
        // pin to all 4 edges
        wkWebView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0.0).isActive = true
        wkWebView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true
        wkWebView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0).isActive = true
        wkWebView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0).isActive = true
        
        
        // load a URL into the WKWebView
//        if let url = URL(string: "https://google.com") {
//            wkWebView.load(URLRequest(url: url))
//        }
        
        let fileName = fileNames[sourceType]!
        if let path = Bundle.main.path(forResource: fileName, ofType: "html") {
            print(path)
            let targetURl = URL(fileURLWithPath: path)
            let request = URLRequest(url: targetURl, cachePolicy: .returnCacheDataElseLoad)
            self.wkWebView.load(request)
        } else {
            print("can't find \(fileName)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WebViewVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUD.show(.progress, onView: self.view)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        HUD.hide()
        
        let nserror = error as NSError
        if nserror.code != NSURLErrorCancelled {
            webView.loadHTMLString("404 - Page Not Found", baseURL: URL(string: "http://www.islamintel.com/"))
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(String(describing: webView.url))
        HUD.hide()
    }
}
