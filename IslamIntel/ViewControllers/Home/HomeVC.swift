//
//  HomeVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 7/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import StoreKit
import PKHUD
import Alamofire
import MessageUI

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //MARK:- IBOutlet
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    @IBOutlet weak var menuBarBtn: UIBarButtonItem!
    //@IBOutlet weak var notificationBarBtn: UIBarButtonItem!
//    @IBOutlet weak var menuBtn: UIButton!
//    @IBOutlet weak var notificationBtn: UIButton!
    
    //MARK:- Property
//    fileprivate var dataList: [String] = [
//        "Prayer Account",
//        "Fasting Account",
//        "Prayer Time Limits",
//        "Spending Tracker",
//        "Tasbih Tracker",
//        ]
    
    fileprivate var dataList: [String] = [
        "Tasbih Tracker"
        ]
    
    var webViewSourceType: SourceType?
    
    fileprivate let CellIdentifier = "HomeCellRID"
    fileprivate let LeaveFeedbackSID = "ShowLeaveFeedbackVC"
    fileprivate let InAppPurchaseSID = "ShowInAppPurchaseVC"
    fileprivate let WebViewSID = "ShowWebViewVC"
    fileprivate let TellFriendsSID = "ShowTellFriendsVC"
    fileprivate let ProfielSettingsTVCSID = "ShowProfielSettingsTVC"
    
    
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScene()
        self.sideMenuController()?.sideMenu?.delegate = self
        
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.checkAndAskReview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func menuBarBtnTapped(_ sender: Any) {
        toggleSideMenuView()
    }
    
//    @IBAction func notificationBarBtnTapped(_ sender: Any) {
//        
//    }
    

    
    //MARK: Helpers
    
    func setupScene() {
        //self.navigationController?.navigationBar.isHidden = true

        homeCollectionView.delegate = self
        homeCollectionView.dataSource = self
        
        
        /*
        // Create flow layout
        let flow = UICollectionViewFlowLayout()
        
        // Define layout constants
        let itemSpacing: CGFloat = 10
        let minimumCellWidth: CGFloat = 130
        let maximumCellWidth: CGFloat = 160
        let collectionViewWidth = homeCollectionView!.bounds.size.width
        
        // Calculate other required constants
        let itemsInOneLine = CGFloat(Int((collectionViewWidth - CGFloat(Int(collectionViewWidth / minimumCellWidth) - 1) * itemSpacing) / minimumCellWidth))
        let width = collectionViewWidth - itemSpacing * (itemsInOneLine - 1)
        let cellWidth = floor(width / itemsInOneLine)
        let realItemSpacing = itemSpacing + (width / itemsInOneLine - cellWidth) * itemsInOneLine / (itemsInOneLine - 1)
        
        // Apply values
        flow.sectionInset = UIEdgeInsets(top: 40, left: 20, bottom: 40, right: 20)
        flow.itemSize = CGSize(width: cellWidth, height: cellWidth)
        flow.minimumInteritemSpacing = realItemSpacing
        flow.minimumLineSpacing = realItemSpacing
        
        // Apply flow layout
        homeCollectionView?.setCollectionViewLayout(flow, animated: false)
        */
    }
    
    func checkAndAskReview(){
        
        let requestReview = AppHandler.shared.checkRatingReview()
        if requestReview {
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
                
                //Save
                UserDefaults.standard.set(Date(), forKey: AppHandler.shared.RatingReviewDateKey)
                UserDefaults.standard.set(true, forKey: AppHandler.shared.RatingReviewCancelKey)
                UserDefaults.standard.synchronize()
            } else {
                self.showRateAlert()
            }
        }
    }
    
    func showRateAlert(){
        let alertController = UIAlertController(title: "Rate \(Constants.buildName)", message: "if you enjoy using \(Constants.buildName), would you mind taking a moment to rate it?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "No, Thanks", style: .default, handler: nil)
        let rateAction = UIAlertAction(title: "Rate \(Constants.buildName)", style: .cancel) { action in
            AppHandler.shared.openRateApp()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(rateAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loadHelpText(){
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SignupVCSegueID" {
            print("SignupVC")
        } else if segue.identifier == WebViewSID {
            guard let sourceType = self.webViewSourceType else {
                return
            }
            let vc = segue.destination as! WebViewVC
            vc.sourceType = sourceType
        }
    }
    
    // MARK: - UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier, for: indexPath) as! HomeCell
        
        let item = dataList[indexPath.row]
        cell.titleLabel.text = item

        if item == "Prayer Account" {
            cell.imageView.image = UIImage(named: "pray")
            cell.badgeView.isHidden = false
            cell.badgeLabel.text = "2"
        } else if item == "Fasting Account" {
            cell.imageView.image = UIImage(named: "fast")
            cell.badgeView.isHidden = true
        } else if item == "Prayer Time Limits" {
            cell.imageView.image = UIImage(named: "time")
            cell.badgeView.isHidden = true
        } else if item == "Spending Tracker" {
            cell.imageView.image = UIImage(named: "spending")
            cell.badgeView.isHidden = false
            cell.badgeLabel.text = "99"
        } else if item == "Tasbih Tracker" {
            cell.imageView.image = UIImage(named: "tasbih")
            cell.badgeView.isHidden = true
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        
        let item = dataList[indexPath.row]
        
        if item == "Tasbih Tracker" {
            
            //let tabBarController = UIStoryboard(name: "Tasbih", bundle: nil).instantiateViewController(withIdentifier: "TasbihTabController") as! UITabBarController
            let inititalViewController = UIStoryboard(name: "Tasbih", bundle: nil).instantiateViewController(withIdentifier: "HomeNavControllerID") as! HomeNavController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = inititalViewController
            
            //self.navigationController?.pushViewController(tabBarController, animated: true)
            
            //
            //self.window?.rootViewController = inititalViewController
            
            //self.present(tabBarController, animated: true, completion: nil)
        }
    }
    
    //collectionviewcellframe
    
}

// MARK: - ENSideMenu Delegate
extension HomeVC: ENSideMenuDelegate {
    
    func sideMenuWillOpen() {
        print("HomeVC: sideMenuWillOpen ")
    }
    
    func sideMenuWillClose() {
        print("HomeVC: sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("HomeVC: sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("HomeVC: sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("HomeVC: sideMenuDidOpen")
    }
}

extension HomeVC: MenuTVCDelegate {
    func menuOptionPressed(menu: MenuOption) {
        switch menu {
        case .aboutUs:
            self.hideSideMenuView()
            self.webViewSourceType = .aboutUs
            self.performSegue(withIdentifier: WebViewSID, sender: self)
            break
        case .leaveFeedback:
            self.hideSideMenuView()
            self.performSegue(withIdentifier: LeaveFeedbackSID, sender: self)
            break
        case .updateToPremium:
            self.hideSideMenuView()
            self.performSegue(withIdentifier: InAppPurchaseSID, sender: self)
            break
        case .tellYourFriend:
            self.hideSideMenuView()
            self.performSegue(withIdentifier: TellFriendsSID, sender: self)
            break
        case .profileSettings:
            self.hideSideMenuView()
            self.performSegue(withIdentifier: ProfielSettingsTVCSID, sender: self)
            break
        case .privacyPolicy:
            self.hideSideMenuView()
            self.webViewSourceType = .privacyPolicy
            self.performSegue(withIdentifier: WebViewSID, sender: self)
            break
        case .reportBug:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["tickets@islamintel.com"])
                mail.setSubject("Ticket")
                //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                
                present(mail, animated: true)
            } else {
                // show failure alert
                let email = "support@islamintel.com"
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            break
        case .visitWebsite:
            if let url = URL(string: "https://www.islamintel.com") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        case .contactUs:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["support@islamintel.com"])
                mail.setSubject("Support")
                //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                
                present(mail, animated: true)
            } else {
                // show failure alert
                let email = "support@islamintel.com"
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            break
        default:
            break
        }
    }
}

extension HomeVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}


