//
//  MenuVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 8/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

enum MenuOption: Int {
    case home
    case aboutUs
    case leaveFeedback
    case updateToPremium
    case tellYourFriend
    case profileSettings
    case privacyPolicy
    case reportBug
    case visitWebsite
    case contactUs
    case empty
    case signOut
}

protocol MenuTVCDelegate: class {
    func menuOptionPressed(menu: MenuOption)
}

class MenuTVC: UITableViewController {
    
    //MARK:- IBOutlet
    weak var delegate: MenuTVCDelegate?
    
    
    @IBOutlet weak var userNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.userNameLabel.text = Session.currentUser!.name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 12
    }
    
    
//     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
//
//        // Configure the cell...
//
//        return cell
//     }
 
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(rgb: 0x4eace9)
        cell.selectedBackgroundView = bgColorView
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("indexPath")
        tableView.deselectRow(at: indexPath, animated: true)
        
        let menuOption = MenuOption(rawValue: indexPath.row)!
        
        switch menuOption {
        case .home:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.openHomeScene(loaderScene: false)
            break
            
        case .signOut:
            let alertController = UIAlertController(title: "Sign out", message: "Are you sure you want to sign out?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (pAlert) in
            })
            let YesAction = UIAlertAction(title: "Yes", style: .default, handler: { (pAlert) in
                //Do whatever you wants here
                Session.currentUser = nil
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.openLoginScene(loaderScene: false)
            })
            alertController.addAction(cancelAction)
            alertController.addAction(YesAction)
            self.present(alertController, animated: true, completion: nil)
            break
        default:
            break
        }
        
        //Tell delegate about it
        self.delegate?.menuOptionPressed(menu: menuOption)
        
        
        /*if indexPath.row == MenuOption.home.rawValue {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.openHomeScene(loaderScene: false)
        }
         
         if indexPath.row == MenuOption.home.hashValue {
            AppHandler.shared.openRateApp()
        }*/
        
    }

//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//
//    }
//
//    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
//
//    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
