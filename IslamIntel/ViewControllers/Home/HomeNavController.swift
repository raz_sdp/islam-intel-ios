//
//  HomeNavController.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 8/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class HomeNavController: ENSideMenuNavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuTVC = storyboard.instantiateViewController(withIdentifier: "MenuTVCID") as! MenuTVC
        
        if let homeVc = self.viewControllers.first as? HomeVC {
            menuTVC.delegate = homeVc
        } else if let tasbihTabController = self.viewControllers.first as? TasbihTabController {
            menuTVC.delegate = tasbihTabController
        }


        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: menuTVC, menuPosition:.left)
        //sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = 290 // optional, default is 160
        sideMenu?.bouncingEnabled = false
        sideMenu?.allowPanGesture = false
        sideMenu?.allowRightSwipe = false
        sideMenu?.allowLeftSwipe = false
        
        //sideMenu?.toggleMenu()
        
        // make navigation bar showing over side menu
        view.bringSubviewToFront(navigationBar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeNavController: ENSideMenuDelegate {
    func sideMenuWillOpen() {
        print("HomeNavController: sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("HomeNavController: sideMenuWillClose")
    }
    
    func sideMenuDidClose() {
        print("HomeNavController: sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("HomeNavController: sideMenuDidOpen")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("HomeNavController: sideMenuShouldOpenSideMenu")
        return true
    }
}
