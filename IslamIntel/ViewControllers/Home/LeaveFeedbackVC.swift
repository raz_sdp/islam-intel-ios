//
//  LeaveFeedbackVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 16/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class LeaveFeedbackVC: UIViewController {
    
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var sendFeedbackBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.feedbackTextView.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.sendFeedbackBtn.setCornerRadius(4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendFeebackBtnTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        guard let feedbackText = feedbackTextView.text, feedbackText.trim().count > 0 else {
            self.feedbackTextView.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter your feedback", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.feedbackTextView.becomeFirstResponder()
            }
            return
        }
        
        guard let userId = Session.currentUser?.userId else {
            self.feedbackTextView.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Whoops something went wrong.", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.feedbackTextView.becomeFirstResponder()
            }
            return
        }
        
        let parameters = [
            "user_id": userId,
            "feedback": feedbackText
        ]
        
        let formattedParameters: [String : AnyObject] = [
            "Feedback": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.Feedback, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.BaseResponse?, error) in
            HUD.hide()
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            guard response.success else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage((response.message ?? "Submitting feedback is failed"), type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Your feedback has been submitted"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                //HUD.dimsBackground = true
                self.backBtnTapped(self)
            }
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
