//
//  LoaderVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 12/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import Alamofire

class LoaderVC: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func loadHelpText(){
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            guard let response = response, error == nil else {
                //HUD.flash(.error, delay: Constants.flashTime)
                //self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }

}
