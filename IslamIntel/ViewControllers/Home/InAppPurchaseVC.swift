//
//  InAppPurchaseVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 16/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import PKHUD

class InAppPurchaseVC: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var purchaseTableView: UITableView!
    @IBOutlet weak var infoLabel: UILabel!

    let appBundleId = "com.islamintel.IslamIntel"
    fileprivate let CellIdentifier = "PurchaseCellRID"
    
    var products: [SKProduct] = []
    

    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        reload()
        self.setupView()
        //getInfo(.TasbihTrackerYearly)
        
        //self.purchaseTableView.refreshControl = UIRefreshControl()
        //self.purchaseTableView.refreshControl?.addTarget(self, action: #selector(InAppPurchaseVC.reload), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func restoreBtnTapped(_ sender: Any) {
        //SwiftyStoreKit
        self.restorePurchases()
    }
    
    func addPurchase(Index: IndexPath) {
        //let product = products[Index.row]
        //IslamIntelProducts.store.buyProduct(product)
        //purchase(.TasbihTrackerYearly, atomically: true)
        
        purchaseSubscriptionAndVerify(.TasbihTrackerYearly, atomically: true)
    }

    //MARK: Swifty

    /*
    @IBAction func autoRenewableGetInfo() {
        getInfo(.TasbihTrackerYearly)
    }
    @IBAction func autoRenewablePurchase() {
        purchase(.TasbihTrackerYearly, atomically: true)
    }
    @IBAction func autoRenewableVerifyPurchase() {
        verifySubscriptions([.TasbihTrackerYearly])
    }
    @IBAction func verifyReceipt() {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(self.alertForVerifyReceipt(result))
        }
    }*/
    
    //MARK:- Helpers
    
    func setupView(){
        self.purchaseTableView.tableFooterView = UIView()
        self.purchaseTableView.dataSource = self
        self.purchaseTableView.delegate = self
        self.purchaseTableView.rowHeight = UITableView.automaticDimension
        self.purchaseTableView.estimatedRowHeight = 44
        
//        let restoreButton = UIBarButtonItem(title: "Restore",
//                                            style: .plain,
//                                            target: self,
//                                            action: #selector(InAppPurchaseVC.restoreTapped(_:)))
//        navigationItem.rightBarButtonItem = restoreButton
        
    }
    
    //MARK: IAPHelper
    @objc func reload() {
        products = []
        self.purchaseTableView.reloadData()
        
        HUD.show(.progress)
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        let productId = appBundleId + "." + RegisteredPurchase.TasbihTrackerYearly.rawValue
        SwiftyStoreKit.retrieveProductsInfo([productId]) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            HUD.hide()
            
            if result.retrievedProducts.count > 0 {
                self.products = Array(result.retrievedProducts)
                self.purchaseTableView.reloadData()
            } else {
                self.showAlert(self.alertForProductRetrievalInfo(result))
            }
        }
        
        /*
        IslamIntelProducts.store.requestProducts{ [weak self] success, products in
            //guard let self = self else { return }
            HUD.hide()
            if success {
                self?.products = products!
                self?.purchaseTableView.reloadData()
            } else {
                
            }
            
            //self?.refreshControl?.endRefreshing()
        }*/
        
        
    }

    
    
    //MARK: SwiftyStoreKit
    func getInfo(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        let productId = "TasbihTrackerYearly"
        SwiftyStoreKit.retrieveProductsInfo([productId]) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(self.alertForProductRetrievalInfo(result))
        }
    }
    
    func restorePurchases() {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        HUD.show(.progress)
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            NetworkActivityIndicatorManager.networkOperationFinished()
            HUD.hide()
            
            for purchase in results.restoredPurchases {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                } else if purchase.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
            
            if results.restoredPurchases.count > 0 {
                self.verifyAutoRenewableSubscription(.TasbihTrackerYearly)
            }
            
            //let expireDate = purchase.transaction.transactionDate?.adjust(.year, offset: 1)
            //UserDefaults.standard.set(expireDate, forKey: purchase.productId)
            
            //UserDefaults.standard.set(true, forKey: purchase.productId)
            //NotificationCenter.default.post(name: SwiftyIAPHelper.restoreSuccessfulNotification, object: purchase.productId)
            
            var message = ""
            var type: GSMessageType = .warning
            if results.restoreFailedPurchases.count > 0 {
                message = "Restore failed! Please contact support"
                type = .error
            } else if results.restoredPurchases.count > 0 {
                message = "All purchases have been restored"
                type = .success
            } else {
                message = "Nothing to restore"
                type = .warning
            }
            
            self.showMessage(message, type: type, options: [.autoHideDelay(Constants.displayTime)])
        }
    }
    

    
    func purchase(_ purchase: RegisteredPurchase, atomically: Bool) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
            if let alert = self.alertForPurchaseResult(result) {
                self.showAlert(alert)
            }
        }
    }
    
    
    
    
    func verifyReceipt() {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(self.alertForVerifyReceipt(result))
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "402e0cc3fcb84abfa7dc0528c14492c5")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func verifyPurchase(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                
                let productId = self.appBundleId + "." + purchase.rawValue
                
                switch purchase {
                case .TasbihTrackerYearly:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt)
                    self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
                default:
                    let purchaseResult = SwiftyStoreKit.verifyPurchase(
                        productId: productId,
                        inReceipt: receipt)
                    self.showAlert(self.alertForVerifyPurchase(purchaseResult, productId: productId))
                }
                
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productIds = Set(purchases.map { self.appBundleId + "." + $0.rawValue })
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: productIds))
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    //MARK:- Auto Renewable Subscription
    
    //Only for auto Renewable Subscription
    func purchaseSubscriptionAndVerify(_ registeredPurchase: RegisteredPurchase, atomically: Bool) {
        
        let productId = appBundleId + "." + registeredPurchase.rawValue
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        HUD.show(.progress)
        //Purchase Subscription
        SwiftyStoreKit.purchaseProduct(productId, atomically: atomically) { purchaseResult in
            NetworkActivityIndicatorManager.networkOperationFinished()
            HUD.hide()
            
            if case .success(let purchase) = purchaseResult {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                //Verify Subscriptions
                self.verifyAutoRenewableSubscription(registeredPurchase)
                
            } else {
                //purchase error
                if let alert = self.alertForPurchaseResult(purchaseResult) {
                    self.showAlert(alert)
                }
            }
        }
    }
    
    func verifyAutoRenewableSubscription(_ registeredPurchase: RegisteredPurchase) {
        
        let productId = appBundleId + "." + registeredPurchase.rawValue
        
        //Verify Subscriptions
        HUD.show(.progress)
        NetworkActivityIndicatorManager.networkOperationStarted()
        self.verifyReceipt { receiptResult in
            NetworkActivityIndicatorManager.networkOperationFinished()
            HUD.hide()
            
            switch receiptResult {
            case .success(let receipt):
                
                let subscriptionResult = SwiftyStoreKit.verifySubscription(ofType: .autoRenewable, productId: productId, inReceipt: receipt)
                
                switch subscriptionResult {
                case .purchased(let expiryDate, let items):
                    print("\(productId) is valid until \(expiryDate)\n\(items)\n")
                    let alert = self.alertWithTitle("Tasbih Tracker Premium is subscribed", message: "This subscription is valid until \(expiryDate)")
                    self.showAlert(alert)
                    
                    //Save subscription details
                    UserDefaults.standard.set(true, forKey: productId)
                    UserDefaults.standard.set(expiryDate, forKey: productId + "expiryDate")
                    
                    //Post Notification
                    NotificationCenter.default.post(name: SwiftyIAPHelper.restoreSuccessfulNotification, object: productId)
                    
                case .expired(let expiryDate, let items):
                    print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                    
                    //Save subscription details
                    UserDefaults.standard.set(false, forKey: productId)
                    UserDefaults.standard.set(expiryDate, forKey: productId + "expiryDate")
                    
                    let alert = self.alertWithTitle("Subscription expired", message: "Subscription is expired since \(expiryDate)")
                    self.showAlert(alert)
                    
                case .notPurchased:
                    print("\(productId) has never been purchased")
                    
                    //Save subscription details
                    UserDefaults.standard.set(false, forKey: productId)
                    UserDefaults.standard.set(nil, forKey: productId + "expiryDate")
                    
                    let alert = self.alertWithTitle("Not subscribed", message: "Tasbih Tracker Premium has never been subscribed")
                    self.showAlert(alert)
                }
                
                //Reload Tableview
                self.purchaseTableView.reloadData()
                
            case .error:
                self.showAlert(self.alertForVerifyReceipt(receiptResult))
            }
        }
    }
}

extension InAppPurchaseVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! PurchaseCell
        
        let product = products[indexPath.row]
        cell.product = product
        
        let action = {
            self.addPurchase(Index: indexPath)
        }
        cell.cellPurchaseBtnTapped = action
        
        return cell
    }
}


// MARK: User facing alerts
extension InAppPurchaseVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let invalidProductId = result.invalidProductIDs.first {
            return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
        } else {
            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
            return alertWithTitle("Could not retrieve product info", message: errorString)
        }
    }
    
    // swiftlint:disable cyclomatic_complexity
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            return nil
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
            }
        }
    }
    
    func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
        
        if results.restoreFailedPurchases.count > 0 {
            print("Restore Failed: \(results.restoreFailedPurchases)")
            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
        } else if results.restoredPurchases.count > 0 {
            print("Restore Success: \(results.restoredPurchases)")
            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
        } else {
            print("Nothing to Restore")
            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success(let receipt):
            print("Verify receipt Success: \(receipt)")
            return alertWithTitle("Receipt verified", message: "Receipt verified remotely")
        case .error(let error):
            print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("Receipt verification", message: "No receipt data. Try again.")
            case .networkError(let error):
                return alertWithTitle("Receipt verification", message: "Network error while verifying receipt: \(error)")
            default:
                return alertWithTitle("Receipt verification", message: "Receipt verification failed: \(error)")
            }
        }
    }
    
    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) -> UIAlertController {
        
        switch result {
        case .purchased(let expiryDate, let items):
            print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
            return alertWithTitle("Product is purchased", message: "Product is valid until \(expiryDate)")
        case .expired(let expiryDate, let items):
            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
            return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
        case .notPurchased:
            print("\(productIds) has never been purchased")
            return alertWithTitle("Not purchased", message: "This product has never been purchased")
        }
    }
    
    func alertForVerifyPurchase(_ result: VerifyPurchaseResult, productId: String) -> UIAlertController {
        
        switch result {
        case .purchased:
            print("\(productId) is purchased")
            return alertWithTitle("Product is purchased", message: "Product will not expire")
        case .notPurchased:
            print("\(productId) has never been purchased")
            return alertWithTitle("Not purchased", message: "This product has never been purchased")
        }
    }
}
