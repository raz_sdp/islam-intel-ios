//
//  TellFriendsVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 16/9/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import FBSDKShareKit
import PKHUD

class TellFriendsVC: UIViewController {
    
    @IBOutlet weak var shareFBBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.shareFBBtn.setCornerRadius(4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareFBBtnTapped(_ sender: Any) {
        
        HUD.show(.progress, onView: self.view)
        
        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.quote = "Hey friends, check out Islam Intel, a great tracking and useful info app"
        content.contentURL = URL(string: Constants.appURL)
        
        
        FBSDKShareDialog.show(from: self, with: content, delegate: self)
        
        //        let dialog = FBSDKShareDialog()
        //        dialog.mode = FBSDKShareDialogMode.automatic
        //        dialog.shareContent = content
        //        dialog.delegate = self as FBSDKSharingDelegate
        //        dialog.fromViewController = self
        //        dialog.show
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension TellFriendsVC: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("didCompleteWithResults: \(String(describing: results))")
        HUD.hide()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        HUD.hide()
        print("didFailWithError: \(String(describing: error))")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        HUD.hide()
        print("sharerDidCancel: \(String(describing: sharer))")
    }
    
    
}
