//
//  CounterTasbihVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import PKHUD
import AudioToolbox
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class CounterTasbihVC: UIViewController {
    
    //MARK:- IBOutlet
    //beadView
    @IBOutlet weak var beadsView: UIView!
    @IBOutlet weak var tasbihCounterLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var tasbihTF: IQDropDownTextField!
    
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var totalUnsavedLabel: UILabel!
    @IBOutlet weak var tasbihCompletedLabel: UILabel!
    
    @IBOutlet weak var statusBarHeightLC: NSLayoutConstraint!

//    @IBOutlet weak var backBtn: UIBarButtonItem!
//    @IBOutlet weak var helpBtn: UIBarButtonItem!
    
    @IBOutlet weak var tasbihInfoBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var saveResetBtn: UIButton!
    @IBOutlet weak var speakerBtn: UIButton!
    
    //MARK: Property
    //private var player: AVAudioPlayer?
    let soundURL: NSURL? = NSURL(fileURLWithPath: Bundle.main.path(forResource: "Tick", ofType: "mp3")!)
    var soundID: SystemSoundID = 0
    fileprivate weak var currentTF: UITextField?
    private var tasbihTapCounter: Int = 0
    private var totalUnsaved: Int = 0
    private var tasbihCompleted: Int = 0
    
    private let maxTasbihCount: Int = 99
    private let tagAdjustment: Int = 100
    
    //Data
    private var tasbihDataList = [Tasbih]()
    private var selectedTasbih: Tasbih?
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getTasbihList()
        self.setupScene()
        
        //Check for Subsciption
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.addCircularBeads()
        print("viewWillAppear")
        //HUD.hide()
        
        self.speakerBtn.isSelected = AppHandler.shared.tasbihCounterSoundEnabled
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.addCircularBeads()
    }
    


    
    //MARK:- IBAction
    
    @IBAction func breadViewTapped(_ sender: Any) {
        
        if totalUnsaved >= Constants.maxTasbihCountLimit {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedCountLimit = numberFormatter.string(from: NSNumber(value:Constants.maxTasbihCountLimit))!
            
            //Check isActiveSubcribedUser
            let tasbihTabController = self.tabBarController as! TasbihTabController
            if !tasbihTabController.isActiveSubcribedUser {
                //Subscribe to Tasbih Tracker Premimium
                let alertController = UIAlertController(title: "Subscription Required", message: "\(formattedCountLimit) limit reached. You need to subscribe to Tasbih Tracker Premium to save data else press Reset to reset the counters all back to 0", preferredStyle: .alert)
                
                let resetAction = UIAlertAction(title: "Reset", style: .default) { action in
                    self.resetBeadView()
                }
                let okAction = UIAlertAction(title: "OK", style: .default) { action in
                    tasbihTabController.menuOptionPressed(menu: .updateToPremium)
                }
                alertController.addAction(resetAction)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            let alert = UIAlertController(title: "\(formattedCountLimit) limit reached.  Save data?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            }))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.resetBeadView()
            }))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        tasbihTapCounter += 1
        totalUnsaved += 1
        
        if tasbihTapCounter == 99 {
            
            tasbihTapCounter = 1
            tasbihCompleted += 1
            
            self.resetBead()
        } else {
            
        }
        
        //Select Bead
        self.selectBead(number: tasbihTapCounter)
        
        self.updateTasbihCounting()
        
        if totalUnsaved > 0 {
            self.resetBtn.isEnabled = true
            self.saveResetBtn.isEnabled = true
        }
        
        //Play Sound
        if self.speakerBtn.isSelected {
            self.playSound()
            //1057, 1103, 1104, 1105, 1306
            //let systemSoundID: SystemSoundID = 1003
            //AudioServicesPlaySystemSound(systemSoundID)
            //AudioServicesPlayAlertSound(SystemSoundID(1306))
        }

    }
    
    @IBAction func editBtnTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Enter primary count number", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Count number"
            textField.font = UIFont(name: "Nunito-Light", size: 16)!
            textField.textColor = UIColor.templateTextFieldTextColor
            textField.keyboardType = .numberPad
            textField.delegate = self
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first, let text = textField.text, let counterNumber = Int(text) else {
                return
            }
            print(counterNumber)
            self.handleCounterNumber(number: counterNumber)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func tasbihInfoBtnTapped(_ sender: Any){
        if let tasbih = self.selectedTasbih {
            let tasbihInfoView = TasbihInfoView()
            tasbihInfoView.delegate = self
            tasbihInfoView.showFullSceen(tasbih: tasbih)
        }
    }
    
    @IBAction func resetBtnTapped(_ sender: Any){
        self.resetBeadView()
    }
    
    @IBAction func saveResetBtnTapped(_ sender: Any){
        guard (Session.currentUser?.userId) != nil else {
            return
        }
        guard self.selectedTasbih != nil else {
            return
        }
        guard self.totalUnsaved > 0 else {
            return
        }
        
        let tasbihTabController = self.tabBarController as! TasbihTabController
        if !tasbihTabController.isActiveSubcribedUser {
            //Subscribe to Tasbih Tracker Premimium
            let alertController = UIAlertController(title: "Subscription Required", message: "To use this premium feature, you need to subscribe to Tasbih Tracker Premium", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let saveAction = UIAlertAction(title: "OK", style: .default) { action in
                tasbihTabController.menuOptionPressed(menu: .updateToPremium)
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        let alertController = UIAlertController(title: "Save", message: "Are you sure you want to save this data to your record?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            self.saveTasbih()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func speakerBtnTapped(_ sender: Any){
        self.speakerBtn.isSelected = !self.speakerBtn.isSelected
        AppHandler.shared.tasbihCounterSoundEnabled = self.speakerBtn.isSelected
    }
    
    
    //MARK: Helpers
    
    func setupScene() {
        
        //self.title = "Tasbih Tracker"
        
        self.addCircularBeads()
        
        //Setup Picker
        self.tasbihTF.delegate = self
        self.tasbihTF.addRightImage("DownTriangle")
        self.tasbihTF.isOptionalDropDown = true
        self.tasbihTF.optionalItemText = "Select"
        self.tasbihTF.textColor = UIColor.templateDarkGray
        self.tasbihTF.optionalItemTextColor = UIColor.templateDarkGray
        self.tasbihTF.dropDownTextColor = UIColor.templateDarkGray
        self.tasbihTF.dropDownFont = UIFont.systemFont(ofSize: 16)
        //self.tasbihTF.itemList = ["Male", "Female"]
        self.tasbihTF.setBorder(color: UIColor.templateDarkGray, width: 1, radius: 4)
        self.tasbihTF.setLeftPadding(width: 8)

        self.saveResetBtn.setCornerRadius(4)
        
        /*let borderWidth: CGFloat = 1
        self.scoreView.frame = self.scoreView.frame.insetBy(dx: -borderWidth, dy: -borderWidth)
        self.scoreView.layer.borderColor = UIColor.templateDarkGray.cgColor
        self.scoreView.layer.borderWidth = borderWidth*/
        
        
        //Initial Setup
        self.resetBeadView()
        
        if selectedTasbih == nil {
            self.beadsView.isUserInteractionEnabled = false
            self.tasbihInfoBtn.isEnabled = false
        } else {
            self.beadsView.isUserInteractionEnabled = true
            self.tasbihInfoBtn.isEnabled = true
        }
        
        self.speakerBtn.isEnabled = true
        //self.speakerBtn.isSelected = true
        
        if UIDevice().isScreen3_5inch() {
            self.statusBarHeightLC.constant = 44
        } else if UIDevice().isScreen4_7inch() || UIDevice().isScreen4inch(){
            self.statusBarHeightLC.constant = 60
        } else {
            self.statusBarHeightLC.constant = 74
        }
        self.view.layoutIfNeeded()
    }
    
    func addCircularBeads(){
        
        let width = UIScreen.main.bounds.width * 0.95
        
        let centerPosition =  CGPoint(x: width/2, y: width/2)
        let maxRadius = width/2
        let centerBtnHeight = (width/2) * 0.3
        let beadHeight = (UIDevice().isScreen4inch() || UIDevice().isScreen3_5inch()) ? 8 : 10
        
        var angle: Double = 180.0
        var radius: CGFloat = centerBtnHeight + 8
        let adjust: CGFloat = (maxRadius - radius)/9
        
        var tag = 101
        
        for line in 0...10 {
            
            angle = 180.0 - Double(line)*(360/11)
            
            for indx in 0...8 {
                
                radius = CGFloat(centerBtnHeight + 8.0 + CGFloat(indx)*adjust)
                
                let x = centerPosition.x + radius * CGFloat(sin(angle * .pi / 180))
                let y = centerPosition.y + radius * CGFloat(cos(angle * .pi / 180))
                
                let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: beadHeight, height: beadHeight))
                imageview.image = UIImage(named: "Bead")
                imageview.highlightedImage = UIImage(named: "BeadFilled")
                imageview.center = CGPoint(x: x, y: y)
                imageview.tag = tag
                self.beadsView.addSubview(imageview)
                
                tag += 1
            }
        }
        
        // Add center Button
        //Added in storyboard
     
        self.beadsView.backgroundColor = .white
    }
    
    func resetBeadView(){
        tasbihTapCounter = 0
        totalUnsaved = 0
        tasbihCompleted = 0
        
        self.resetBtn.isEnabled = false
        self.saveResetBtn.isEnabled = false
        
        self.updateTasbihCounting()
        self.resetBead()
    }
    
    func updateTasbihCounting(){
        
        self.tasbihCounterLabel.text = String(format: "%02d", tasbihTapCounter)
        self.tasbihCompletedLabel.text = String(format: "%d", tasbihCompleted)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedTotalUnsaved = numberFormatter.string(from: NSNumber(value: totalUnsaved))
        self.totalUnsavedLabel.text = formattedTotalUnsaved
    }
    
    func selectBead(number: Int){
        
        let tag = number + tagAdjustment
        
        for view in beadsView.subviews {
            if let imageview = view as? UIImageView, imageview.tag == tag {
                imageview.isHighlighted = true
                return
            }
        }
    }
    
    func selectBead(tillNumber: Int){
        let tag = tillNumber + 100
        for view in beadsView.subviews {
            if let imageview = view as? UIImageView, imageview.tag > tagAdjustment, imageview.tag <= tag {
                imageview.isHighlighted = true
            }
        }
    }
    
    func resetBead(){
        
        //beadsView.subviews.m
        //beadsView.subviews.filter({$0.tag != 321}).forEach({$0.removeFromSuperview()})
        
        for view in beadsView.subviews {
            if let imageview = view as? UIImageView, imageview.tag > tagAdjustment {
                imageview.isHighlighted = false
            }
        }
    }
    
    func playSound() {
        if let url = soundURL {
            AudioServicesCreateSystemSoundID(url, &soundID)
            AudioServicesPlaySystemSound(soundID)
        }
    }
    
    func handleCounterNumber(number: Int) {
        
        if number >= Constants.maxTasbihCountLimit {
            self.totalUnsaved = Constants.maxTasbihCountLimit
            self.tasbihCompleted = Constants.maxTasbihCountLimit/99
            self.tasbihTapCounter = Constants.maxTasbihCountLimit%99
            
            //Select Bead
            self.selectBead(tillNumber: tasbihTapCounter)
            self.updateTasbihCounting()
            
            if totalUnsaved > 0 {
                self.resetBtn.isEnabled = true
                self.saveResetBtn.isEnabled = true
            } else {
                self.resetBtn.isEnabled = false
                self.saveResetBtn.isEnabled = false
            }
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedCountLimit = numberFormatter.string(from: NSNumber(value:Constants.maxTasbihCountLimit))!
            
            //Check isActiveSubcribedUser
            let tasbihTabController = self.tabBarController as! TasbihTabController
            if !tasbihTabController.isActiveSubcribedUser {
                //Subscribe to Tasbih Tracker Premimium
                let alertController = UIAlertController(title: "Subscription Required", message: "\(formattedCountLimit) limit reached. You need to subscribe to Tasbih Tracker Premium to save data else press Reset to reset the counters all back to 0", preferredStyle: .alert)
                
                let resetAction = UIAlertAction(title: "Reset", style: .default) { action in
                    self.resetBeadView()
                }
                let okAction = UIAlertAction(title: "OK", style: .default) { action in
                    tasbihTabController.menuOptionPressed(menu: .updateToPremium)
                }
                alertController.addAction(resetAction)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            //Warning
            let alert = UIAlertController(title: "\(formattedCountLimit) limit exceeded.  Save data?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            }))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                //self.resetBeadView()
                self.saveResetBtnTapped(alert)
            }))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        self.totalUnsaved = number
        self.tasbihCompleted = number/99
        self.tasbihTapCounter = number%99
        
        //Select Bead
        self.selectBead(tillNumber: tasbihTapCounter)
        self.updateTasbihCounting()
        
        if totalUnsaved > 0 {
            self.resetBtn.isEnabled = true
            self.saveResetBtn.isEnabled = true
        } else {
            self.resetBtn.isEnabled = false
            self.saveResetBtn.isEnabled = false
        }
        
        //Play Sound
        if self.speakerBtn.isSelected {
            self.playSound()
        }
    }
    

    
    
    //MARK: API Helpers
    func getTasbihList(){
        
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetTasbihList, encoding: URLEncoding.default) { (response: TasbihList?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }

            //Save Data
            self.tasbihDataList = response.tasbihList
            let list = response.tasbihList.map({ "\($0.id!). \($0.transliteration!) (\($0.meaning!))"})
            self.tasbihTF.itemList = list
        }
    }
    
    func saveTasbih(){
        
        guard let userId = Session.currentUser?.userId else {
            return
        }
        guard let tasbih = self.selectedTasbih else {
            return
        }
        guard self.totalUnsaved > 0 else {
            return
        }
        
        let parameters = [
            "user_id": userId,
            "tasbihoption_id": tasbih.id,
            "count": self.totalUnsaved,
            ] as [String : AnyObject]
        
        let formattedParameters: [String : AnyObject] = [
            "Tasbihhistory": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.AddTasbihCount, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.BaseResponse?, error) in
            HUD.hide()
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            
            if response.success {
                HUD.flash(.success, delay: Constants.flashTime)
                self.showMessage(response.message ?? "Successfully saved", type: .success, options: [.autoHideDelay(Constants.displayTime)])
                let tab = self.tabBarController as! TasbihTabController
                tab.performanceUpdateAvailable = true
                self.resetBeadView()
            } else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(response.message ?? "Something went wrong", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            }
        }
    }
    
    //MARK: Facebook
    func handleFBButtonTap() {
        guard let tasbih = self.selectedTasbih, let imageUrlStr = tasbih.image else {
            return
        }
        
        let imageCache = AutoPurgingImageCache()
        if let tasbihImage = imageCache.image(withIdentifier: imageUrlStr) {
            print("image cached: \(tasbihImage)")
            self.fbSharePhoto(image: tasbihImage)
        } else {
            HUD.show(.progress, onView: self.view)
            Alamofire.request(imageUrlStr).responseImage { response in
                //HUD.hide(animated: true)
                debugPrint(response)
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let tasbihImage = response.result.value {
                    print("image downloaded: \(tasbihImage)")
                    imageCache.add(tasbihImage, withIdentifier: imageUrlStr)
                    self.fbSharePhoto(image: tasbihImage)
                }
            }
        }
    }
    
    func fbSharePhoto(image: UIImage){
        
        HUD.show(.progress, onView: self.view)
        
        let photo: FBSDKSharePhoto = FBSDKSharePhoto()
        photo.image = image
        photo.isUserGenerated = true
        
        let content: FBSDKSharePhotoContent = FBSDKSharePhotoContent()
        content.photos = [photo]
        
//        let dialog = FBSDKShareDialog()
//        dialog.mode = FBSDKShareDialogMode.automatic
//        dialog.shareContent = content
//        dialog.delegate = self as FBSDKSharingDelegate
//        dialog.fromViewController = self
//        dialog.show
        
        FBSDKShareDialog.show(from: self, with: content, delegate: self)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension CounterTasbihVC: IQDropDownTextFieldDelegate {
    
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {

        if let item = item, item != "Select" {
            let tasbih = self.tasbihDataList.first{(item.contains($0.transliteration!))}
            if let tasbih = tasbih {
                print(tasbih.transliteration)
                self.selectedTasbih = tasbih
            } else {
                self.selectedTasbih = nil
            }
        } else {
            self.selectedTasbih = nil
        }
        
        self.tasbihInfoBtn.isEnabled = (selectedTasbih != nil)
        self.beadsView.isUserInteractionEnabled = (selectedTasbih != nil)
        self.resetBeadView()
    }
}

extension CounterTasbihVC {
    
    func circleOfDots() {
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 200,y: 200), radius: CGFloat(100), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        let point = CGPoint(x: self.view.bounds.midX-200, y: self.view.bounds.midY-200)
        shapeLayer.position = point
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.blue.cgColor
        
        //you can change the line width
        shapeLayer.lineWidth = 10.0
        let one : NSNumber = 1
        let two : NSNumber = 56
        shapeLayer.lineDashPattern = [one,two]
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        
        self.view.layer.addSublayer(shapeLayer)
    }
    
    func newCircles() {
        
        // your dot diameter.
        let dotDiameter: CGFloat = 15.0
        
        // your 'expected' dot spacing. we'll try to get as closer value to this as possible.
        //let expDotSpacing: CGFloat = 56.0
        
        // the size of your view
        let size: CGSize = view.frame.size
        
        // the radius of your circle, half the width or height (whichever is smaller) with the dot radius subtracted to account for stroking
        let radius: CGFloat = (size.width < size.height) ? size.width * 0.5 - dotDiameter * 0.5 : size.height * 0.5 - dotDiameter * 0.5
        
        // the circumference of your circle
        let circum: CGFloat = .pi * radius * 2.0
        
        // the number of dots to draw as given by the circumference divided by the diameter of the dot plus the expected dot spacing.
        //let numberOfDots: Int = Int(round(circum / (dotDiameter + expDotSpacing)))
        let numberOfDots: Int = 11
        
        
        // the calculated dot spacing, as given by the circumference divided by the number of dots, minus the dot diameter.
        let dotSpacing: CGFloat = (circum / CGFloat(numberOfDots)) - dotDiameter
        
        // your shape layer
        let layer = CAShapeLayer()
        layer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        // set to the diameter of each dot
        layer.lineWidth = dotDiameter
        // your stroke color
        layer.strokeColor = UIColor.blue.cgColor
        
        //layer.strokeColor
        
        // the circle path - given the center of the layer as the center and starting at the top of the arc.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: size.width*0.5, y: size.height*0.5), radius: radius, startAngle: -.pi * 0.5, endAngle: .pi * 1.5, clockwise: true)
        layer.path = circlePath.cgPath
        
        // prevent that layer from filling the area that the path occupies
        layer.fillColor = UIColor.clear.cgColor
        
        // round shape for your stroke
        layer.lineCap = CAShapeLayerLineCap.round
        
        // 0 length for the filled segment (radius calculated from the line width), dot diameter plus the dot spacing for the un-filled section
        let one: NSNumber = 0
        let two: NSNumber = (dotSpacing + dotDiameter) as NSNumber
        layer.lineDashPattern = [one, two]
        view.layer.addSublayer(layer)
    }
}

extension CounterTasbihVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
            return false
        }
        
        let newLength = text.count + string.count - range.length
        return newLength <= 20
    }
}

extension CounterTasbihVC: TasbihInfoViewDelegate {
    func didTapOkutton() {
        
    }
    
    func didTapFacebookButton() {
        self.handleFBButtonTap()
    }
}

extension CounterTasbihVC: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("didCompleteWithResults: \(String(describing: results))")
        HUD.hide()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        HUD.hide()
        print("didFailWithError: \(String(describing: error))")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        HUD.hide()
        print("sharerDidCancel: \(String(describing: sharer))")
    }
    

}
