//
//  SettingsTasbihTVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class SettingsTasbihTVC: UITableViewController  {
    
    @IBOutlet weak var maxLimitLabel: UILabel!
    @IBOutlet weak var soundSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        soundSwitch.isOn = AppHandler.shared.tasbihCounterSoundEnabled
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:Constants.maxTasbihCountLimit))
        maxLimitLabel.text = formattedNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func soundSwitchValueChanged(_ sender: UISwitch) {
        AppHandler.shared.tasbihCounterSoundEnabled = sender.isOn
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.cellForRow(at: indexPath)!
//        if indexPath.row == 1 {
//
//            for view in cell.contentView.subviews {
//                if view.isKind(of: UILabel.self), view.tag == 10{
//                    let label = view as! UILabel
//                    label.text = String(Constants.maxTasbihCountLimit)
//                }
//            }
//        }
//        
//        return cell
//    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(rgb: 0x4eace9)
        cell.selectedBackgroundView = bgColorView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("indexPath")
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
