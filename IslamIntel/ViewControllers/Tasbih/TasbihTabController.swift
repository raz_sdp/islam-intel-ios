//
//  TasbihTabController.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 21/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import MessageUI
import StoreKit
import SwiftyStoreKit

class TasbihTabController: UITabBarController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var menuBarBtn: UIBarButtonItem!
    @IBOutlet weak var notificationBarBtn: UIBarButtonItem!
    
    public var performanceUpdateAvailable: Bool = false
    let pagetitles = ["Counter", "Performance", "Settings"]
    
    let tasbihProductId = Constants.purchaseAppBundleId + "." + RegisteredPurchase.TasbihTrackerYearly.rawValue
    internal private(set) var isActiveSubcribedUser: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tasbih Tracker"
         self.sideMenuController()?.sideMenu?.delegate = self
        
        self.notificationBarBtn.image = UIImage(named:"help")?.withRenderingMode(.alwaysOriginal)
        
        checkSubscription()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TasbihTabController.restoreSuccessful(notification:)), name: SwiftyIAPHelper.restoreSuccessfulNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Subscription
    
    func checkSubscription(){
        let productId = Constants.purchaseAppBundleId + "." + RegisteredPurchase.TasbihTrackerYearly.rawValue
        
        guard let expiryDate = UserDefaults.standard.value(forKey: productId + "expiryDate") as? Date else {
            self.isActiveSubcribedUser = false
            return
        }
        
        if Date().compare(.isEarlier(than: expiryDate)) {
            self.isActiveSubcribedUser = true
            return
            
        } else {
            HUD.show(.progress)
            SwiftyIAPHelper.shared.verifyAutoRenewableSubscription(.TasbihTrackerYearly) { (receiptResult, subscriptionResult) in
                HUD.hide()
                if let subscriptionResult = subscriptionResult {
                    switch subscriptionResult {
                    case .purchased(let expiryDate, let items):
                        print("\(productId) is valid until \(expiryDate)\n\(items)\n")
                        
                        //Save subscription details
                        UserDefaults.standard.set(true, forKey: productId)
                        UserDefaults.standard.set(expiryDate, forKey: productId + "expiryDate")
                        
                    case .expired(let expiryDate, let items):
                        print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                        
                        //Save subscription details
                        UserDefaults.standard.set(false, forKey: productId)
                        UserDefaults.standard.set(expiryDate, forKey: productId + "expiryDate")
                        
                    case .notPurchased:
                        print("\(productId) has never been purchased")
                        
                        //Save subscription details
                        UserDefaults.standard.set(false, forKey: productId)
                        UserDefaults.standard.set(nil, forKey: productId + "expiryDate")
                    }
                } else {
                    self.showAlert(self.alertForVerifyReceipt(receiptResult))
                }
            }
        }
    }
    
    @objc private func restoreSuccessful(notification: Notification) {
        guard let productId = notification.object as? String, productId == tasbihProductId else { return }
        
        guard
            UserDefaults.standard.bool(forKey: productId),
            let expiryDate = UserDefaults.standard.value(forKey: productId + "expiryDate") as? Date
            else {
                self.isActiveSubcribedUser = false
                return
        }
        
        self.isActiveSubcribedUser = Date().compare(.isEarlier(than: expiryDate))
    }
    
    //MARK:- IBAction
    
    @IBAction func menuBarBtnTapped(_ sender: Any) {
        toggleSideMenuView()
    }
    
    @IBAction func notificationBarBtnTapped(_ sender: Any) {
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
            return
        }
        
        let pageTitle = self.pagetitles[self.selectedIndex]
        
        guard let help = AppHandler.shared.helpText!.getHelpText(pageTitle: pageTitle) else {
            return
        }
        
        self.view.endEditing(true)
        
        let infoView = InfoView()
        infoView.setup(frame: self.view.bounds, help: help)
        self.view.addSubview(infoView)
        return
    }
    
    
    func loadHelpText(){
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }

    
    // MARK: - Navigation

}

// MARK: - ENSideMenu Delegate
extension TasbihTabController: ENSideMenuDelegate {
    
    func sideMenuWillOpen() {
        print("TasbihTabController: sideMenuWillOpen ")
    }
    
    func sideMenuWillClose() {
        print("TasbihTabController: sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("TasbihTabController: sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("TasbihTabController: sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("TasbihTabController: sideMenuDidOpen")
    }
}

// MARK: - Left Menu
// MARK: MenuTVCDelegate
extension TasbihTabController: MenuTVCDelegate {
    
    func menuOptionPressed(menu: MenuOption) {
        switch menu {
        case .aboutUs:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let webViewVC = storyboard.instantiateViewController(withIdentifier: "WebViewVCID") as! WebViewVC
            webViewVC.sourceType = .aboutUs
            self.navigationController?.pushViewController(webViewVC, animated: true)
            break
        case .leaveFeedback:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let leaveFeedbackVC = storyboard.instantiateViewController(withIdentifier: "LeaveFeedbackVCID") as! LeaveFeedbackVC
            self.navigationController?.pushViewController(leaveFeedbackVC, animated: true)
            break
        case .updateToPremium:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let inAppPurchaseVC = storyboard.instantiateViewController(withIdentifier: "InAppPurchaseVCID") as! InAppPurchaseVC
            self.navigationController?.pushViewController(inAppPurchaseVC, animated: true)
            break
        case .tellYourFriend:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let tellFriendsVC = storyboard.instantiateViewController(withIdentifier: "TellFriendsVCID") as! TellFriendsVC
            self.navigationController?.pushViewController(tellFriendsVC, animated: true)
            break
        case .profileSettings:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let profielSettingsTVC = storyboard.instantiateViewController(withIdentifier: "ProfielSettingsTVCID") as! ProfielSettingsTVC
            self.navigationController?.pushViewController(profielSettingsTVC, animated: true)
            break
        case .privacyPolicy:
            self.hideSideMenuView()
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let webViewVC = storyboard.instantiateViewController(withIdentifier: "WebViewVCID") as! WebViewVC
            webViewVC.sourceType = .privacyPolicy
            self.navigationController?.pushViewController(webViewVC, animated: true)
            break
        case .reportBug:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["tickets@islamintel.com"])
                mail.setSubject("Ticket")
                //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                
                present(mail, animated: true)
            } else {
                // show failure alert
                let email = "support@islamintel.com"
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            break
        case .visitWebsite:
            if let url = URL(string: "https://www.islamintel.com") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        case .contactUs:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["support@islamintel.com"])
                mail.setSubject("Support")
                //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                
                present(mail, animated: true)
            } else {
                // show failure alert
                let email = "support@islamintel.com"
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            break
        default:
            break
        }
    }
}

// MARK: MFMailComposeViewControllerDelegate
extension TasbihTabController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// MARK: User facing alerts
extension TasbihTabController {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }

    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success(let receipt):
            print("Verify receipt Success: \(receipt)")
            return alertWithTitle("Receipt verified", message: "Receipt verified remotely")
        case .error(let error):
            print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("Receipt verification", message: "No receipt data. Try again.")
            case .networkError(let error):
                return alertWithTitle("Receipt verification", message: "Network error while verifying receipt: \(error)")
            default:
                return alertWithTitle("Receipt verification", message: "Receipt verification failed: \(error)")
            }
        }
    }
    
    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) -> UIAlertController {
        
        switch result {
        case .purchased(let expiryDate, let items):
            print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
            return alertWithTitle("Product is purchased", message: "Product is valid until \(expiryDate)")
        case .expired(let expiryDate, let items):
            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
            return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
        case .notPurchased:
            print("\(productIds) has never been purchased")
            return alertWithTitle("Not purchased", message: "This product has never been purchased")
        }
    }
    
    func alertForVerifyPurchase(_ result: VerifyPurchaseResult, productId: String) -> UIAlertController {
        
        switch result {
        case .purchased:
            print("\(productId) is purchased")
            return alertWithTitle("Product is purchased", message: "Product will not expire")
        case .notPurchased:
            print("\(productId) has never been purchased")
            return alertWithTitle("Not purchased", message: "This product has never been purchased")
        }
    }
}

