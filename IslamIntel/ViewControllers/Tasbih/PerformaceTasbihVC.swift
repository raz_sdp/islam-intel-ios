//
//  PerformaceTasbihVC
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class PerformaceTasbihVC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    //MARK: Property
    var shouldHideData: Bool = false
    
    var spinner: UIActivityIndicatorView?
    var pageViewController: UIPageViewController!
    var pages = [HorizontalBarTasbihVC]()
    
    private var pageCount = 0
    private var currentIndex = 0
    private var pendingIndex = 0
    
    var tappedHistoryTimeType: TimeType?
    var hasUpdateForTimeType: TimeType?
    var defaultSorting = true
    
    //Data
    let performancePages: [TimeType] = [.today, .week, .month, .year, .overall]
    
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tab = self.tabBarController as! TasbihTabController
        if tab.performanceUpdateAvailable {
            for vc in self.pages {
                vc.newUpdateAvailable = true
            }
            tab.performanceUpdateAvailable = false
            
            //updateAvailable = updateAvailable.mapValues { _ in true }
            //updateAvailable.map { (key, value) in (key, !value) }
        }
        
        if self.hasUpdateForTimeType != nil {
            for vc in self.pages {
                vc.newUpdateAvailable = true
            }
            self.hasUpdateForTimeType = nil
         }
        
        /*if let timeType = self.hasUpdateForTimeType {
            if let index = self.pages.index(where: {$0.timeType == timeType}) {
                let horizontalBarTasbihVC = self.pages[index]
                horizontalBarTasbihVC.getTasbihCountList()
            }
            self.hasUpdateForTimeType = nil
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowEditHistoryVC" {
            let editHistoryVC = segue.destination as! EditHistoryVC
            editHistoryVC.timeType = self.tappedHistoryTimeType!
            editHistoryVC.delegate = self
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func previousBtnTapped(_ sender: Any){
        self.flip(toPage: currentIndex-1, animated: true)
    }
    
    @IBAction func nextBtnTapped(_ sender: Any){
        self.flip(toPage: currentIndex+1, animated: true)
    }
    
    //MARK:- Helpers
    
    func setupScene(){
        
        //self.title = "Tasbih Tracker" 
        
        guard self.pageViewController == nil else {
            let indexPath = IndexPath(item: 0, section: 0)
            self.flip(toPage: indexPath.row, animated: false)
            
            return
        }
        
        self.pageCount = performancePages.count
        self.currentIndex = 0
        self.pendingIndex = -1
        
        let storyboard = UIStoryboard(name: "Tasbih", bundle: nil)
        
        for (index, timeType) in performancePages.enumerated() {
            
            let horizontalBarTasbihVC = storyboard.instantiateViewController(withIdentifier: "HorizontalBarTasbihVCID") as! HorizontalBarTasbihVC
            horizontalBarTasbihVC.pageIndex = index
            horizontalBarTasbihVC.timeType = timeType
            horizontalBarTasbihVC.delegate = self
            
            pages.append(horizontalBarTasbihVC)
        }
        
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        self.pageViewController.setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
        
        
        self.addChild(pageViewController)
        self.containerView.addSubview(pageViewController.view)
        //self.containerView.insertSubview(pageViewController.view, belowSubview: pageControl)
        pageViewController.didMove(toParent: self)
        
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.containerView.addConstraints([
            NSLayoutConstraint(item: self.pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.containerView, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.containerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.containerView, attribute: .trailing, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.containerView, attribute: .leading, multiplier: 1.0, constant: 0),
            ])
    }
    


    
    //MARK: API Helpers


    
}

extension PerformaceTasbihVC: HorizontalBarTasbihVCDelegate {
    func btnTapped(button: Int) {
    }
    
    func historyBtnTapped(timeType: TimeType) {
        self.tappedHistoryTimeType = timeType
        self.performSegue(withIdentifier: "ShowEditHistoryVC", sender: self)
    }
}

extension PerformaceTasbihVC: EditHistoryVCDelegate {
    func editHistoryDeleted(timeType: TimeType) {
        
        self.hasUpdateForTimeType = timeType
    }
}

//MARK:- UIPageViewController DataSource, Delegate
extension PerformaceTasbihVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let horizontalBarTasbihVC = viewController as? HorizontalBarTasbihVC {
            let currentPageIndex = horizontalBarTasbihVC.pageIndex
            
            if currentPageIndex == 0 {
                return nil
            }
            
            let previousPageIndex = (currentPageIndex - 1) % self.pages.count
            return self.pages[previousPageIndex]
        }
        
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let horizontalBarTasbihVC = viewController as? HorizontalBarTasbihVC {
            let currentPageIndex = horizontalBarTasbihVC.pageIndex
            
            if currentPageIndex == self.pages.count-1 {
                return nil
            }
            
            let nextPageIndex = (currentPageIndex + 1) % self.pages.count
            return self.pages[nextPageIndex]
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        if let pendingViewController = pendingViewControllers.first, let horizontalBarTasbihVC = pendingViewController as? HorizontalBarTasbihVC {
            self.pendingIndex = horizontalBarTasbihVC.pageIndex
        } else {
            self.pendingIndex = -1
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            if pendingIndex > -1 {
                currentIndex = pendingIndex
                
                if currentIndex == 0 {
                    self.timeLabel.text = "Today"
                } else if currentIndex == 1 {
                    self.timeLabel.text = "This Week"
                } else if currentIndex == 2 {
                    self.timeLabel.text = "This Month"
                } else if currentIndex == 3 {
                    self.timeLabel.text = "This Year"
                } else if currentIndex == 4 {
                    self.timeLabel.text = "Overall"
                }
                
                //let indexPath = IndexPath(item: pendingIndex, section: 0)
                //self.selectCell(collectionView: stickerGroupCollectionView, indexPath: indexPath)
                //stickerGroupCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: (currentIndex > pendingIndex) ? .left : .right)
                //pageControl.currentPage = currentIndex
            }
        }
    }
    
    func flip(toPage index: Int, animated: Bool = true) {
        //guard
        if index == currentIndex || pageCount == 1 || index >= pageCount || index < 0 {
            return
        }
        
        if currentIndex < index {
            currentIndex = index
            pageViewController.setViewControllers([pages[currentIndex]], direction: .forward, animated: animated) { complete in
                if complete {
                    if self.currentIndex == 0 {
                        self.timeLabel.text = "Today"
                    } else if self.currentIndex == 1 {
                        self.timeLabel.text = "This Week"
                    } else if self.currentIndex == 2 {
                        self.timeLabel.text = "This Month"
                    } else if self.currentIndex == 3 {
                        self.timeLabel.text = "This Year"
                    } else if self.currentIndex == 4 {
                        self.timeLabel.text = "Overall"
                    }
                }
            }
        } else if currentIndex > index {
            currentIndex = index
            pageViewController.setViewControllers([pages[currentIndex]], direction: .reverse, animated: animated) { complete in
                if complete {
                    if self.currentIndex == 0 {
                        self.timeLabel.text = "Today"
                    } else if self.currentIndex == 1 {
                        self.timeLabel.text = "This Week"
                    } else if self.currentIndex == 2 {
                        self.timeLabel.text = "This Month"
                    } else if self.currentIndex == 3 {
                        self.timeLabel.text = "This Year"
                    } else if self.currentIndex == 4 {
                        self.timeLabel.text = "Overall"
                    }
                }
            }
        }
    }
    
}
