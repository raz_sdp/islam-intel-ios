//
//  EditHistoryVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 31/8/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

protocol EditHistoryVCDelegate: class {
    func editHistoryDeleted(timeType: TimeType)
}

class EditHistoryVC: UIViewController {
    
    //MARK:- IBOutlet
    
    @IBOutlet weak var editTasbihTableView: UITableView!
    
    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertDeleteBtn: UIButton!
    @IBOutlet weak var alertCancelBtn: UIButton!

    //MARK: Property
    weak var delegate: EditHistoryVCDelegate?
    
    private let CellIdentifier = "EditTasbihHistoryCellRID"
    
    //Data
    var tasbihHistoryOptionList = [TasbihHistoryOption]()
    var timeType: TimeType = .today
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupScene()
        self.getTasbihHistory()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editBtnTapped(_ sender: UIBarButtonItem){
        
        guard self.tasbihHistoryOptionList.count > 0 else {
            sender.isEnabled = false
            sender.title = "Edit"
            self.deleteBtn.isEnabled = false
            return
        }
        
        let editing: Bool = (sender.title! == "Edit")
        sender.title = editing ? "Cancel" : "Edit"

        self.editTasbihTableView.setEditing(editing, animated: true)
        
        if !editing {
            //let hasSelectedAtleastOne = (editTasbihTableView.indexPathsForSelectedRows?.count ?? 0) > 0
            self.deleteBtn.isEnabled = false
            self.deleteBtn.backgroundColor = UIColor(rgb: 0xdddddd, alpha: 1.0)
            
            /*if !self.alertView.isHidden{
                self.alertView.hideWithAnimation(true)
            }*/
        }
    }
    
    @IBAction func deleteBtnTapped(_ sender: UIButton){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete selected record(s)", style: .destructive) { deleteAction in
            self.alertDeleteBtnTapped(UIButton())
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { deleteAction in
            
        }
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        self.present(alert, animated: true, completion: nil)
        
        return
            
        //self.alertView.hideWithAnimation(false)
    }
    
    @IBAction func alertCancelBtnTapped(_ sender: UIButton){
        //self.alertView.hideWithAnimation(true)
    }
    
    @objc func alertViewTapped(_ sender: UIGestureRecognizer){
        //self.alertView.hideWithAnimation(true)
    }
    
    @IBAction func alertDeleteBtnTapped(_ sender: UIButton){
        print("deleteBtnTapped")
        
        guard let selectedIndexPaths = self.editTasbihTableView.indexPathsForSelectedRows, selectedIndexPaths.count > 0 else {
            return
        }
        
        var selectedIds = [String]()
        for indexPath in selectedIndexPaths {
            let tasbihHistoryOption = self.tasbihHistoryOptionList[indexPath.section]
            let tasbihHistory = tasbihHistoryOption.tasbihHistoryList[indexPath.row]
            selectedIds.append(tasbihHistory.id!)
        }
        
        let parameters = [
            "id": selectedIds
            ] as [String : AnyObject]
        
        let formattedParameters: [String : AnyObject] = [
            "Tasbihhistory": parameters
            ] as [String : AnyObject]
        
        
        //self.alertView.hideWithAnimation(true)
        HUD.show(.progress)
        
        Request.send(.post, path: Constants.API.DeleteTasbihHistory, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.SuccessResponse?, error) in
            HUD.hide()
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            if response.success {
                HUD.flash(.success, delay: Constants.flashTime)
                let record = selectedIds.count > 1 ? "Record" : "Records"
                self.showMessage("\(record) deleted", type: .success, options: [.autoHideDelay(Constants.displayTime)])
                
                //Removed Data
                var deletedTasbihHistoryList: [Int : [TasbihHistory]] = [Int : [TasbihHistory]]()
                
                for indexPath in selectedIndexPaths {
                    let tasbihHistoryOption = self.tasbihHistoryOptionList[indexPath.section]
                    let tasbihHistory = tasbihHistoryOption.tasbihHistoryList[indexPath.row]
                    
                    if deletedTasbihHistoryList[indexPath.section] == nil {
                        deletedTasbihHistoryList[indexPath.section] = [tasbihHistory]
                    } else {
                        deletedTasbihHistoryList[indexPath.section]?.append(tasbihHistory)
                    }
                }
                
                //Delete Rows: TasbihHistory
                var deletedTasbihHistoryOption = [TasbihHistoryOption]()
                for (section, tasbihHistoryList) in deletedTasbihHistoryList {
                    let tasbihHistoryOption = self.tasbihHistoryOptionList[section]
                    
                    for tasbihHistory in tasbihHistoryList {
                        if let index = tasbihHistoryOption.tasbihHistoryList.index(where: {$0.id == tasbihHistory.id}) {
                            tasbihHistoryOption.tasbihHistoryList.remove(at: index)
                        }
                    }
                    
                    if tasbihHistoryOption.tasbihHistoryList.count == 0 {
                        deletedTasbihHistoryOption.append(tasbihHistoryOption)
                    }
                }
                
                //Delete Sections: TasbihHistoryOption
                for tasbihHistoryOption in deletedTasbihHistoryOption {
                    if let index = self.tasbihHistoryOptionList.index(where: {$0.optionName == tasbihHistoryOption.optionName}) {
                        self.tasbihHistoryOptionList.remove(at: index)
                    }
                }
                
                self.editBtnTapped(self.rightBarButton)
                self.editTasbihTableView.reloadData()

                self.delegate?.editHistoryDeleted(timeType: self.timeType)
                
                /*
                 self.editTasbihTableView.beginUpdates()
                 self.editTasbihTableView.setEditing(false, animated: false)
                 self.editTasbihTableView.deleteRows(at: selectedIndexPaths, with: .automatic)
                 self.editTasbihTableView.deleteSections(indexSet, with: .automatic)
                 self.editTasbihTableView.endUpdates()*/
                
            } else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage("Something went wrong", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            }
        }
    }
    
    //MARK:- Helpers
    
    func setupScene() {
        
        self.editTasbihTableView.delegate = self
        self.editTasbihTableView.dataSource = self
        self.editTasbihTableView.allowsSelection = false
        self.editTasbihTableView.allowsMultipleSelectionDuringEditing = true
        
        self.editTasbihTableView.sectionFooterHeight = 0.0
        
        //self.navigationController?.title =
        var timeText: String!
        switch self.timeType {
        case .today:
            timeText = "Today"
        case .week:
            timeText = "This Week"
        case .month:
            timeText = "This Month"
        case .year:
            timeText = "This Year"
        case .overall:
            timeText = "Overall"
        }
        
        self.navigationItem.title = "History - \(timeText!)"
        
        self.deleteBtn.setCornerRadius(6.0)
        self.deleteBtn.isEnabled = false
        self.deleteBtn.backgroundColor = UIColor(rgb: 0xdddddd, alpha: 1.0)
        rightBarButton.title = "Edit"
    
        let font: UIFont = UIFont(name: "Nunito-Regular", size: 18)!
        let attributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font : font]
        rightBarButton.setTitleTextAttributes(attributes, for: .normal)
    
        
        self.alertView.isHidden = true
        //let tap = UITapGestureRecognizer(target: self, action: #selector(self.alertViewTapped(_:)))
        //tap.cancelsTouchesInView = true
        //self.alertView.addGestureRecognizer(tap)
        
        self.alertCancelBtn.setCornerRadius(8.0)
        self.alertDeleteBtn.setCornerRadius(8.0)
    }
    
    //MARK: API Helpers
    func getTasbihHistory(){
        guard let userId = Session.currentUser?.userId else {
            return
        }
        
        HUD.show(.progress, onView: self.view)
        Request.send(.get, path: Constants.API.GetEditTasbihHistory(userId: userId, timeType: timeType), encoding: URLEncoding.default) { (response: EditTasbihHistory?, error) in
            HUD.hide()
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            if response.success, let countList = response.tasbihHistoryOptionList, countList.count > 0 {
                print(countList)
                self.tasbihHistoryOptionList = response.tasbihHistoryOptionList
                self.editTasbihTableView.reloadData()
            } else {
                self.rightBarButton.isEnabled = false
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditHistoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let numOfSections = self.tasbihHistoryOptionList.count
        
        if numOfSections == 0 {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No data available"
            noDataLabel.textColor = UIColor.templateGray
            noDataLabel.font = UIFont(name: "Nunito-Light", size: 16)!
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            
        } else {
            tableView.backgroundView = nil
        }
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let tasbihHistoryOption = self.tasbihHistoryOptionList[section]
        let numberOfRows = tasbihHistoryOption.tasbihHistoryList.count
        
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tasbihHistoryOption = self.tasbihHistoryOptionList[indexPath.section]
        let tasbihHistory = tasbihHistoryOption.tasbihHistoryList[indexPath.row]
        print(tasbihHistory)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! EditTasbihHistoryCell
        
        cell.historyLabel.text = "\(tasbihHistory.count!) on \(tasbihHistory.createdDate.toString(format: .custom("dd MMM yyyy @ hh:mm a")))"
        
        //cell.selectionStyle = tableView.isEditing ? .default : .none
        //cell.accessoryType = .checkmark
        let clearView = UIView()
        clearView.backgroundColor = .clear
        cell.selectedBackgroundView = clearView
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            let hasSelectedAtleastOne = (tableView.indexPathsForSelectedRows?.count ?? 0) > 0
            self.deleteBtn.isEnabled = hasSelectedAtleastOne
            self.deleteBtn.backgroundColor = hasSelectedAtleastOne ? UIColor(rgb: 0xbcbcbc, alpha: 1.0) : UIColor(rgb: 0xdddddd, alpha: 1.0)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            let hasSelectedAtleastOne = (tableView.indexPathsForSelectedRows?.count ?? 0) > 0
            self.deleteBtn.isEnabled = hasSelectedAtleastOne
            self.deleteBtn.backgroundColor = hasSelectedAtleastOne ? UIColor(rgb: 0xbcbcbc, alpha: 1.0) : UIColor(rgb: 0xdddddd, alpha: 1.0)
        }
    }
    
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 60.0))
        headerView.backgroundColor = UIColor(rgb: 0xebebeb, alpha: 1) 
        let headerLabel = UILabel(frame: CGRect(x: 16, y: 0, width: tableView.bounds.size.width, height: 60.0))
        headerLabel.text = self.tableView(self.editTasbihTableView, titleForHeaderInSection: section)
        headerLabel.font = UIFont(name: "Nunito-Bold", size: 18)!
        headerLabel.textColor = UIColor(rgb: 0x777777, alpha: 1)
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let tasbihHistoryOption = self.tasbihHistoryOptionList[section]
        return tasbihHistoryOption.optionName
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0.0
//    }
}
