//
//  HorizontalBarTasbihVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 28/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

protocol HorizontalBarTasbihVCDelegate: class {
    func btnTapped(button: Int)
    func historyBtnTapped(timeType: TimeType)
}

class HorizontalBarTasbihVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var overallBarView: UIView!
    @IBOutlet weak var tasbihTableView: UITableView!
    
    @IBOutlet weak var sortBtn: UIButton!
    @IBOutlet weak var seeHistoryBtn: UIButton!
    
    //MARK: Property
    weak var delegate: HorizontalBarTasbihVCDelegate?
    var pageIndex = 0
    var shouldReset = false
    var timeType: TimeType = .today
    var newUpdateAvailable: Bool = false
    var firstTimeAppearing: Bool = true
    
    private let CellIdentifier = "TasbihBarCellRID"
    private let CellIdentifierOverall = "TasbihBarOverallCellRID"
    var spinner: UIActivityIndicatorView?
    
    var maxTasbihValue: Int = 0
    
    //Data
    var countList = [TasbihCount]()
    var overallCountList = [OverallTasbihCount]()
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tasbihTableView.delegate = self
        self.tasbihTableView.dataSource = self
        
        self.setupScene()
        self.getTasbihCountList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if newUpdateAvailable && !firstTimeAppearing {
            self.getTasbihCountList()
            newUpdateAvailable = false
        }
        
        if firstTimeAppearing {
            firstTimeAppearing = false
        }
        
        self.sortTashbiCountList(defaultSorting: AppHandler.shared.deafultSorting)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    
    @IBAction func sortBtnTapped(_ sender: UIButton){
        AppHandler.shared.deafultSorting = !AppHandler.shared.deafultSorting
        self.sortTashbiCountList(defaultSorting: AppHandler.shared.deafultSorting)
    }
    
    @IBAction func seeHistoryBtnTapped(_ sender: UIButton){
        self.delegate?.historyBtnTapped(timeType: self.timeType)
    }
    
    //MARK:- Helpers
    
    func sortTashbiCountList(defaultSorting: Bool) {
        
        if timeType == .overall {
            guard overallCountList.count > 0 else { return }
            
            var list = overallCountList

            var listWithCount = list.filter { $0.overallCount > 0}
            var listWithOutCount = list.filter { $0.overallCount <= 0}
            
            //Sort count
            if defaultSorting {
                listWithCount.sort { $0.overallCount > $1.overallCount }
                listWithOutCount.sort { Int($0.transliteration_id!)! < Int($1.transliteration_id!)! }
                
                list = listWithCount
                list.append(contentsOf: listWithOutCount)
            } else {
                //Reverse
                listWithCount.sort { $0.overallCount < $1.overallCount }
                listWithOutCount.sort { Int($0.transliteration_id!)! > Int($1.transliteration_id!)! }
                
                list = listWithOutCount
                list.append(contentsOf: listWithCount)
            }
            
            overallCountList = list
        } else {
            guard countList.count > 0 else { return }
            var list = countList
            var listWithCount = list.filter { $0.count > 0}
            var listWithOutCount = list.filter { $0.count <= 0}
            
            //Sort count
            if defaultSorting {
                listWithCount.sort { $0.count > $1.count }
                listWithOutCount.sort { Int($0.transliteration_id!)! < Int($1.transliteration_id!)! }
                
                list = listWithCount
                list.append(contentsOf: listWithOutCount)
            } else {
                //Reverse
                listWithCount.sort { $0.count < $1.count }
                listWithOutCount.sort { Int($0.transliteration_id!)! > Int($1.transliteration_id!)! }
                
                list = listWithOutCount
                list.append(contentsOf: listWithCount)
            }
            
            countList = list
        }
        
        self.tasbihTableView.reloadData()
        self.tasbihTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
    
    func setupScene() {
        self.seeHistoryBtn.setCornerRadius(6.0)
        if self.timeType == .overall {
            self.overallBarView.isHidden = false
        } else {
            
        self.overallBarView.isHidden = true
        }
    }
    
    func showActivityIndicatorView(_ show: Bool) {
        if show {
            if self.spinner == nil {
                let viewSize: CGSize = self.view.frame.size
                let spinner = UIActivityIndicatorView(style: .whiteLarge)
                spinner.color = UIColor.gray
                spinner.center = CGPoint(x: viewSize.width / 2, y: viewSize.height / 2)
                spinner.hidesWhenStopped = true
                view.addSubview(spinner)
                
                self.spinner = spinner
                
            } else {
                if spinner!.isAnimating {
                    return
                }
            }
            self.spinner?.startAnimating()
            view.isUserInteractionEnabled = false
            
        } else {
            
            //hide
            view.isUserInteractionEnabled = true
            self.spinner?.stopAnimating()
            self.spinner?.removeFromSuperview()
        }
    }
    
    
    //MARK: API Helpers
    func getTasbihCountList(){
        guard let userId = Session.currentUser?.userId else {
            return
        }
        
        HUD.show(.progress, onView: self.view)
        
        if self.timeType == .overall {
            Request.send(.get, path: Constants.API.GetCountTasbih(userId: userId, timeType: timeType), encoding: URLEncoding.default) { (response: OverallTasbihCountList?, error) in
                HUD.hide()
                guard let response = response, error == nil else {
                    HUD.flash(.error, delay: Constants.flashTime)
                    self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                    return
                }
                
                if response.success, let countList = response.countList, countList.count > 0 {
                    print(countList)
                    
                    let maxValue = countList.map{ $0.overallCount }.max()
                    if let maxValue = maxValue {
                        self.maxTasbihValue = maxValue
                    }
                    
                    self.overallCountList = countList
                    self.sortTashbiCountList(defaultSorting: AppHandler.shared.deafultSorting)
                    //self.tasbihTableView.reloadData()
                }
            }
        } else {
            Request.send(.get, path: Constants.API.GetCountTasbih(userId: userId, timeType: timeType), encoding: URLEncoding.default) { (response: TasbihCountList?, error) in
                HUD.hide()
                guard let response = response, error == nil else {
                    HUD.flash(.error, delay: Constants.flashTime)
                    self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                    return
                }
                
                if response.success, let countList = response.countList, countList.count > 0 {
                    print(countList)
                    
                    let maxValue = countList.map{ $0.count }.max()
                    if let maxValue = maxValue {
                        self.maxTasbihValue = maxValue
                    }
                    self.countList = countList
                    self.sortTashbiCountList(defaultSorting: AppHandler.shared.deafultSorting)
                    //self.tasbihTableView.reloadData()
                }
            }
        }
    }
    
}

extension HorizontalBarTasbihVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        
        if self.timeType == .overall {
            numOfSections = (self.overallCountList.count > 0) ? 1 : 0
        } else {
            numOfSections = (self.countList.count > 0) ? 1 : 0
        }
        
        if numOfSections == 0 {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No data available"
            noDataLabel.textColor = UIColor.templateGray
            noDataLabel.font = UIFont(name: "Nunito-Light", size: 16)!
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
    
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.timeType == .overall) ? overallCountList.count : countList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.timeType == .overall {
            let overallTasbihCount = overallCountList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifierOverall, for: indexPath) as! TasbihBarOverallCell
            cell.transliterationLabel.text = overallTasbihCount.transliteration
            cell.setupBarWidth(overallTasbihCount: overallTasbihCount, maxValue: maxTasbihValue)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.topVerticalLine.isHidden = (indexPath.row == 0)
            cell.bottomVerticalLine.isHidden = (indexPath.row == (overallCountList.count - 1))
            
            return cell
        } else {
            let tasbihCount = countList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! TasbihBarCell
            cell.transliterationLabel.text = tasbihCount.transliteration
            cell.setupBarWidth(value: tasbihCount.count!, maxValue: maxTasbihValue)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.topVerticalLine.isHidden = (indexPath.row == 0)
            cell.bottomVerticalLine.isHidden = (indexPath.row == (countList.count - 1))
            
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.timeType == .overall ? 90.0 : 40.0
    }
    
}
