//
//  MaxLimitTVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/8/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

class MaxLimitTVC: UITableViewController {
    
    enum MaxLimitOption: Int {
        case one = 1000
        case five = 5000
        case ten = 10000
    }
    
    var selectedIndexPath: IndexPath = IndexPath(row: 2, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let selectedLimit = Constants.maxTasbihCountLimit
        
        if selectedLimit == 1000 {
            selectedIndexPath = IndexPath(row: 0, section: 0)
        } else if selectedLimit == 5000 {
            selectedIndexPath = IndexPath(row: 1, section: 0)
        } else {
            selectedIndexPath = IndexPath(row: 2, section: 0)
        }
        
        let cell = self.tableView.cellForRow(at: selectedIndexPath)
        cell?.accessoryType = .checkmark
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(rgb: 0x4eace9)
        cell.selectedBackgroundView = bgColorView
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("indexPath")
        tableView.deselectRow(at: indexPath, animated: true)
        
        var selectedLimit: Int = 0
        
        if indexPath.row == 0 {
            selectedLimit = 1000
        } else if indexPath.row == 1 {
            selectedLimit = 5000
        } else if indexPath.row == 2 {
            selectedLimit = 10000
        }
        
        AppHandler.shared.setMaxLimitUnsaved(value: selectedLimit)
        
        let prevCell = tableView.cellForRow(at: selectedIndexPath)
        prevCell?.accessoryType = .none
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        
        selectedIndexPath = indexPath
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
