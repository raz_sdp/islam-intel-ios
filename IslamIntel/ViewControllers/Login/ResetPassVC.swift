//
//  ResetPassVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 7/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class ResetPassVC: UIViewController, UITextFieldDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var emailTF: TextField!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var helpBtn: UIBarButtonItem!
    @IBOutlet weak var resetPassBtn: UIButton!
    
    let pageTitle = "Forgot Password"
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScene()
        
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func backBtnTapped(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpBtnTapped(_ sender: Any){
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
            return
        }
        
        guard let help = AppHandler.shared.helpText!.getHelpText(pageTitle: self.pageTitle) else {
            return
        }
        
        self.view.endEditing(true)
        let infoView = InfoView()
        infoView.setup(frame: self.view.bounds, help: help)
        self.view.addSubview(infoView)
        return
    }
    
    @IBAction func resetPassBtnTapped(_ sender: Any){
        guard let email = emailTF.text, email.trim().count > 0 else {
            self.emailTF.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter your email address", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        if !email.isValidEmail() {
            self.emailTF.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter a valid email address", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }

        let parameters = [
            "email": email
        ]
        
        let formattedParameters: [String : AnyObject] = [
            "User": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.ResetPassword, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.BaseResponse?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard response.success else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage((response.message ?? "Reset failed"), type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Reset successful"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    
    //MARK: Helpers
    
    func setupScene() {
        self.helpBtn.image = UIImage(named:"help")?.withRenderingMode(.alwaysOriginal)
        
        self.emailTF.addHolderImage("message")
        self.emailTF.setBottomBorder()
        self.resetPassBtn.setCornerRadius(4)
    }
    
    func loadHelpText(){
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }
    
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("ShouldBeginEditing: \(textField.tag)")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("DidBeginEditing: \(textField.tag)")
        textField.layer.shadowColor = UIColor.templateBase.cgColor
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("ShouldEndEditing: \(textField.tag)")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing: \(textField.tag)")
        textField.layer.shadowColor = UIColor.borderGray.cgColor
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        return newLength <= 50
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
