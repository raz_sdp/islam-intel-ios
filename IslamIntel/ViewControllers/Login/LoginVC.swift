//
//  LoginVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 5/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit

class LoginVC: UIViewController, UITextFieldDelegate {
    
    static let isSignupByFBKey = "isSignupByFBKey"
    
    //MARK:- IBOutlet
    @IBOutlet weak var emailTF: TextField!
    @IBOutlet weak var passwordTF: TextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var forgetPassBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    
    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let session = Session.persisted {
            self.emailTF.text = session.userEmail
            self.passwordTF.text = session.userPass
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func loginBtnTapped(_ sender: Any){
        
        self.view.endEditing(true)
        
        guard let email = emailTF.text, email.trim().count > 0 else {
            self.emailTF.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter your email address", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        if !email.isValidEmail() {
            self.emailTF.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter a valid email address", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        guard let password = passwordTF.text, password.trim().count > 0 else {
            self.passwordTF.layer.shadowColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter your password", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(3.0) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        
        self.login(email: email, password: password)
    }
    
    
    @IBAction func signupBtnTapped(_ sender: Any){
        self.view.endEditing(true)
        self.performSegue(withIdentifier: "SignupVCSegueID", sender: sender)
    }
    
    @IBAction func forgetPassBtnTapped(_ sender: Any){
        self.view.endEditing(true)
        self.performSegue(withIdentifier: "ResetPassVCSegueID", sender: sender)
    }
    
    @IBAction func facebookBtnTapped(_ sender: Any){
        self.view.endEditing(true)
        print("facebookBtnTapped")
        
        //fbLoginBtn.sendActions(for: .touchUpInside)
        
        //Facebook Login Manager
        if let currentAccessToken = FBSDKAccessToken.current(), !currentAccessToken.isExpired  {
            
            guard currentAccessToken.hasGranted("user_birthday"), currentAccessToken.hasGranted("user_gender") else {
                print("User doesn't give permission: user_birthday, user_gender")
                self.callFBLoginAPI()
                return
            }
            
            self.loginByFB()
            
        } else {
            
            self.callFBLoginAPI()
        }
    }
    
    
    
    //MARK:- Helpers
    
    func setupScene() {
        
        self.emailTF.addHolderImage("message")
        self.emailTF.setBottomBorder()
        
        self.passwordTF.addHolderImage("password")
        self.passwordTF.setBottomBorder()
        
        self.loginBtn.setCornerRadius(4)
        self.facebookBtn.setCornerRadius(4)
        self.signupBtn.setBorder(color: UIColor.templateBase, width: 1, radius: 4)
    }
    
    
    func login(email: String, password: String, fbLogin:Bool = false){
        let parameters = [
            "email": email,
            "password": password
        ]
        
        let formattedParameters: [String : AnyObject] = [
            "User": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.Login, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.LoginResponse?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard response.success else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage((response.message ?? "Login failed"), type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard let user = response.user else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage("Something went wrong!", type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            Session.currentUser = user
            if fbLogin {
                Session.currentFBUser = FacebookUser(email: email, pass: password)
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: LoginVC.isSignupByFBKey)
                defaults.synchronize()
            } else {
                Session.current = Session(tokenId: "", userId: "", password: password, email: email)
            }
            
            //HUD.dimsBackground = false
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Login successful"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                //HUD.dimsBackground = true
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.openHomeScene()
            }
        }
    }
    
    //MARK: Facebook Login
    func callFBLoginAPI() {
        
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["public_profile","email", "user_gender", "user_birthday"], from: self) { (result, error) in
            
            if (error != nil) {
                print("Process error")
            } else if (result?.isCancelled)! {
                print("Cancelled")
            } else {
                print("Logged in")
                guard let result = result else {
                    print("Something went wrong")
                    return
                }
                
                if let currentAccessToken = FBSDKAccessToken.current() {
                    print("currentAccessToken: \(String(describing: currentAccessToken.userID))")
                    
                    guard result.grantedPermissions.contains("user_birthday"), result.grantedPermissions.contains("user_gender") else {
                        print("User doesn't give permission: user_birthday, user_gender")
                        HUD.flash(.error, delay: Constants.flashTime)
                        self.showMessage("Please give permission to your birthday & gender info", type: .error, options: [.autoHideDelay(Constants.displayTime)])
                        return
                    }
                    
                    self.loginByFB()
                }
            }
        }
    }
    
    
    func loginByFB(){
        let defaults = UserDefaults.standard
        let isSignupByFB = defaults.bool(forKey: LoginVC.isSignupByFBKey)
        
        //defaults.synchronize()
        if isSignupByFB, let fbUser = Session.persistedFacebookUser {
            // FB Login
            self.login(email: fbUser.email, password: fbUser.pass, fbLogin: true)
            
        } else {
            //Graph Call > (Signup > Login) / Login
            self.startGraphRequest()
        }
    }
    
    
    fileprivate func signupWithFB(params: [String: String]){
        
        let formattedParameters: [String : AnyObject] = [
            "User": params
            ] as [String : AnyObject]
        Request.send(.post, path: Constants.API.SignUp, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.LoginResponse?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.labeledError(title: "Error", subtitle: error?.localizedDescription), delay: 3.0, completion: { completed in
                })
                return
            }
            
            guard response.success else {
                self.login(email: params["email"]!, password: params["password"]!, fbLogin: true)
                return
            }
            
            guard let user = response.user else {
                HUD.flash(.labeledError(title: "Login Failed", subtitle: "Something went wrong!"), delay: 3.0, completion: { completed in
                })
                return
            }
            
            Session.currentUser = user
            let email = params["email"]!
            let pass = params["password"]!
            Session.currentFBUser = FacebookUser(email: email, pass: pass)
            
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: LoginVC.isSignupByFBKey)
            defaults.synchronize()
            
            //HUD.dimsBackground = false
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Login successful"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                //HUD.dimsBackground = true
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.openHomeScene()
            }
        }
    }
    
    fileprivate func startGraphRequest(){
        guard let currentAccessToken = FBSDKAccessToken.current()  else {
            return
        }
        
        let params = ["fields": "email, name, first_name, last_name, gender, birthday"]
        
        HUD.show(.progress)
        //user_birthday, first_name, last_name, bio, picture.type(large)
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        let _ = graphRequest?.start(completionHandler: { (connection, userinfo, error) in
            
            guard error == nil, let userinfo = userinfo as? [String: Any] else {
                print(error?.localizedDescription ?? "")
                HUD.flash(.labeledError(title: "Error", subtitle: error?.localizedDescription), delay: 3.0)
                return
            }
            
            print("userinfo: ", userinfo)
            
            guard let email = userinfo["email"] as? String else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage("Error: this facebook account is created without email", type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            
            let defaults = UserDefaults.standard
            let isSignupByFB = defaults.bool(forKey: LoginVC.isSignupByFBKey)
            
            let password = currentAccessToken.userID ?? currentAccessToken.appID!
            
            //defaults.synchronize()
            if isSignupByFB {
                //Login
                self.login(email: email, password: password, fbLogin: true)
                
            } else {
                
                //Signup
                let name = userinfo["name"] as! String
                let gender = (userinfo["gender"] as? String) ?? "male"
                let birthday = (userinfo["birthday"] as? String) ?? "01/31/2000"
                let birthDayStr = Date(fromString: birthday, format: .custom("MM/dd/yyyy"))?.toString(format: .custom("yyyy-MM-dd")) ?? "2000-01-31"
                
                let parameters = [
                    "fullname": name,
                    "dob": birthDayStr,
                    "gender": gender,
                    "email": email,
                    "password": password
                ]
                
                self.signupWithFB(params: parameters)
            }
        })
    }
    
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SignupVCSegueID" {
            print("SignupVC")
        } else if segue.identifier == "ResetPassVCSegueID" {
            print("ResetPassVC")
        }
    }
    
    //MARK:- UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("ShouldBeginEditing: \(textField.tag)")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("DidBeginEditing: \(textField.tag)")
        textField.layer.shadowColor = UIColor.templateBase.cgColor
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("ShouldEndEditing: \(textField.tag)")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing: \(textField.tag)")
        textField.layer.shadowColor = UIColor.borderGray.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.emailTF {
            self.passwordTF.becomeFirstResponder()
        } else if textField == self.passwordTF {
            self.loginBtnTapped(textField)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        return newLength <= 50
    }
}

/*
 extension LoginVC: FBSDKLoginButtonDelegate {
 
 func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
 
 guard error == nil else {
 print(error.localizedDescription)
 return
 }
 
 if result.isCancelled {
 print("Cancelled")
 } else {
 print("Logged in")
 if let currentAccessToken = FBSDKAccessToken.current() {
 print(currentAccessToken.userID)
 print(currentAccessToken.tokenString)
 }
 
 if result.grantedPermissions.contains("user_birthday") {
 print("Successful to get permission: user_birthday")
 
 self.startGraphRequest()
 } else {
 self.startGraphRequest()
 }
 }
 }
 
 
 /*func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool (
 }*/
 
 func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
 
 }
 }*/
