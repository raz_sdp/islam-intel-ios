//
//  SignupVC.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 5/7/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire


class SignupVC: UIViewController, UITextFieldDelegate, IQActionSheetPickerViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dateOfBirthTF: IQDropDownTextField!
    @IBOutlet weak var genderTF: IQDropDownTextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var helpBtn: UIBarButtonItem!
    @IBOutlet weak var signupBtn: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var confirmPassLabel: UILabel!
    
    //MARK:- Property
    fileprivate weak var currentTF: UITextField?
    fileprivate var birthDate: Date?
    
    let pageTitle = "Sign Up"

    //MARK:- View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupScene()
        
        //self.navigationController?.navigationBar.isHidden = true
        
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.nameTF.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func backBtnTapped(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpBtnTapped(_ sender: Any){
        if AppHandler.shared.helpText == nil {
            self.loadHelpText()
            return
        }
        
        guard let help = AppHandler.shared.helpText!.getHelpText(pageTitle: self.pageTitle) else {
            return
        }
        
        self.view.endEditing(true)
        
        let infoView = InfoView()
        infoView.setup(frame: self.view.bounds, help: help)
        self.view.addSubview(infoView)
        return
    }
    
    @IBAction func signupBtnTapped(_ sender: Any){
        
        guard let name = nameTF.text, name.trim().count > 0 else {
            self.nameTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("Please enter your name", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.nameTF.becomeFirstResponder()
            }
            return
        }
        
        let minDate = Date().adjust(.year, offset: -9)
        guard let birthDate = dateOfBirthTF.date, birthDate.compare(.isEarlier(than: minDate)) else {
            self.dateOfBirthTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("can't be less than 9 years", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.dateOfBirthTF.becomeFirstResponder()
            }
            return
        }
        
        guard let gender = genderTF.selectedItem, gender.trim().count > 0 else {
            self.genderTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("must be entered", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.genderTF.becomeFirstResponder()
            }
            return
        }
        
        guard let email = emailTF.text, email.trim().count > 0 else {
            self.emailTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("please enter your email address", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        if !email.isValidEmail() {
            self.emailTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("invalid (e.g. joe.bloggs@gmail.com)", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        guard let password = passwordTF.text, password.count > 3 else {
            self.passwordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("4-12 charactes allowed", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        
        guard password.count == password.trim().count else {
            self.passwordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("white spaces aren't allowed", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        
        guard let confirmPassword = confirmPasswordTF.text, password == confirmPassword else {
            self.confirmPasswordTF.layer.borderColor = UIColor.templateWaning.cgColor
            self.showMessage("password doesn't match", type: .error, options: [.autoHideDelay(Constants.displayTime)])
            delay(Constants.displayTime) {
                self.confirmPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        let birthDayStr = birthDate.toString(format: .custom("yyyy-MM-dd"))
        //let gender = genderTF.selectedItem!.lowercased()
        
        /// API Call
        
        let parameters = [
            "fullname": name,
            "dob": birthDayStr,
            "gender": gender.lowercased(),
            "email": email,
            "password": password
        ]
        
        let formattedParameters: [String : AnyObject] = [
            "User": parameters
            ] as [String : AnyObject]
        
        HUD.show(.progress)
        Request.send(.post, path: Constants.API.SignUp, parameters: formattedParameters, encoding: URLEncoding.default) { (response: Response.LoginResponse?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard response.success else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage((response.message ?? "Signup failed"), type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            guard let user = response.user else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage("Something went wrong!", type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            Session.currentUser = user
            Session.current = Session(tokenId: "", userId: "", password: password, email: email)
            
            HUD.flash(.success, delay: Constants.flashTime)
            self.showMessage((response.message ?? "Signup successful"), type: .success, options: [.autoHideDelay(Constants.flashTime)])
            self.delay(Constants.flashTime) {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.openHomeScene()
            }
        }
    }
    
    //MARK: Helpers
    
    
    
    func setupScene() {
        self.helpBtn.image = UIImage(named:"help")?.withRenderingMode(.alwaysOriginal)
        
        //Setup Picker
        //self.dateOfBirthTF.selectedItem = ""
        self.dateOfBirthTF.isOptionalDropDown = true
        self.dateOfBirthTF.optionalItemTextColor = UIColor.templateGray
        //self.dateOfBirthTF.dropDownTextColor = UIColor.templateGray
        //self.dateOfBirthTF.dropDownFont = UIFont(name: "Nunito-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
        self.dateOfBirthTF.dropDownFont = UIFont.systemFont(ofSize: 16)
    
        self.dateOfBirthTF.placeholder = "DD MON YYYY"
        self.dateOfBirthTF.dropDownMode = .datePicker
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyy"
        self.dateOfBirthTF.dateFormatter = formatter
        self.dateOfBirthTF.delegate = self
        //let date = Date(fromString: "2000-01-01", format: .isoDate)!
        //self.dateOfBirthTF.selectedItem = Date().toString(format: .custom("dd MMM yyy"))

        //Setup Picker
        self.genderTF.isOptionalDropDown = true
        self.genderTF.optionalItemTextColor = UIColor.templateGray
        //self.genderTF.dropDownTextColor = UIColor.templateGray
        self.genderTF.dropDownFont = UIFont.systemFont(ofSize: 16)
        
        self.genderTF.itemList = ["Male", "Female"]
        
        
        self.nameTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.dateOfBirthTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.genderTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.emailTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.passwordTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        self.confirmPasswordTF.setBorder(color: UIColor.borderGray, width: 1, radius: 4)
        
        self.nameTF.setLeftPadding(width: 12)
        self.dateOfBirthTF.setLeftPadding(width: 12)
        self.genderTF.setLeftPadding(width: 12)
        self.emailTF.setLeftPadding(width: 12)
        self.passwordTF.setLeftPadding(width: 12)
        self.confirmPasswordTF.setLeftPadding(width: 12)
        
        self.signupBtn.setCornerRadius(4)
        
        fontSetup(nameLabel)
        fontSetup(dobLabel)
        fontSetup(genderLabel)
        fontSetup(emailLabel)
        fontSetup(passwordLabel)
        fontSetup(confirmPassLabel)
    }
    
    func fontSetup(_ label: UILabel){
        let attributedString = NSMutableAttributedString(string: label.text!)
        
        let attributes0: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor.templateGray,
            .font: UIFont(name: "Nunito-Light", size: 16)!
        ]
        attributedString.addAttributes(attributes0, range: NSRange(location: 0, length: (label.text!.count-1)))
        
        let attributes1: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor.templateBase
        ]
        attributedString.addAttributes(attributes1, range: NSRange(location: (label.text!.count-1), length: 1))
        
        label.attributedText = attributedString
    }
    
    func loadHelpText(){
        HUD.show(.progress)
        Request.send(.get, path: Constants.API.GetHelp, encoding: URLEncoding.default) { (response: HelpText?, error) in
            HUD.hide()
            
            guard let response = response, error == nil else {
                HUD.flash(.error, delay: Constants.flashTime)
                self.showMessage(error!.localizedDescription, type: .error, options: [.autoHideDelay(Constants.displayTime)])
                return
            }
            
            //Save Data
            AppHandler.shared.helpText = response
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("ShouldBeginEditing: \(textField.tag)")
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("DidBeginEditing: \(textField.tag)")
        textField.layer.borderColor = UIColor.templateBase.cgColor
        self.currentTF = textField
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("ShouldEndEditing: \(textField.tag)")
        
        if textField == self.nameTF {
            
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing: \(textField.tag)")
        textField.layer.borderColor = UIColor.borderGray.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        
        if textField == self.nameTF {
            self.dateOfBirthTF.becomeFirstResponder()
        } else if textField == self.emailTF {
            self.passwordTF.becomeFirstResponder()
        } else if textField == self.passwordTF {
            self.confirmPasswordTF.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField == self.nameTF {
            return newLength <= 30
        }
        
        if textField == self.emailTF {
            return newLength <= 40
        }
        
        if textField == self.passwordTF || textField == self.confirmPasswordTF {
            return newLength <= 12
        }
        
        return newLength <= 50
    }
}

extension SignupVC: IQDropDownTextFieldDelegate {
    
    func textField(_ textField: IQDropDownTextField, didSelect date: Date?) {
        
        textField.selectedItem = textField.selectedItem?.uppercased()
    }
}
