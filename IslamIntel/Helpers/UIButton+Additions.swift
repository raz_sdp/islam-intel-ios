//
//  UIButton+Additions.swift
//  UIButton+Additions
//
//  Created by iOS Mac Mini on 1/17/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

extension UIButton {
    
    func rightCornerRoundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomRight , .topRight], cornerRadii: CGSize(width: 6.0, height: 6.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
}
