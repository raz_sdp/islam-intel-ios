//
//  UIImage.swift
//  UIImage
//
//  Created by Mr. Hasan on 8/3/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    class func imageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
