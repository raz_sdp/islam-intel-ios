//
//  UITextField+Additions.swift
//  GeoBit
//
//  Created by Mr. Hasan on 1/17/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

extension UITextField {

    //var isShowingWarning = false
    
    func showWarning(message: String){
        self.layer.borderColor = UIColor.templateRed.cgColor
        self.backgroundColor = UIColor.templateWaning
        self.textColor = UIColor.templateRed
        self.text = message
        if self.isSecureTextEntry {
            self.isSecureTextEntry = false
        }
    }
    
    func leftCornerRoundedTextField(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .topLeft], cornerRadii: CGSize(width: 6.0, height: 6.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
        
    }
    
//    func setCornerRadius(_ radius: CGFloat){
//        self.layer.cornerRadius = radius
//        self.clipsToBounds = true
//    }
    
    func setLeftPadding(width: Double){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 10))
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = view
        //self.addSubview(view)
    }
    
    func addHolderImage(_ image: String){
        
        let image = UIImage(named: image)
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame = CGRect(x: 0, y: 10, width: 25, height: 25)
        imageView.contentMode = .scaleAspectFit
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = imageView
        self.addSubview(imageView)
    }
    
    func addRightImage(_ image: String){
        let image = UIImage(named: image)
        let imageView = UIImageView()
        imageView.image = image
        
        let size = self.bounds.size
        let x: CGFloat = 0
        let y: CGFloat = size.height/2 - 5
        
        imageView.frame = CGRect(x: x, y: y, width: 30, height: 10)
        imageView.contentMode = .left
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = imageView
        self.addSubview(imageView)
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.borderGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
}
