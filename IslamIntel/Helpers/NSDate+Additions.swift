//
//  NSDate+Additions.swift
//  NSDate+Additions
//
//  Created by MacBook on 6/6/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import UIKit

extension NSDate {
    
    convenience
    init(dateString:String) {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyyMMdd hhmmss"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "fr_CH_POSIX") as Locale
        if let d = dateStringFormatter.date(from: dateString)
        {
            self.init(timeInterval: 0, since: d)
        }
        else
        {
            self.init(timeInterval: 0, since: NSDate() as Date)
        }
    }
    
}
