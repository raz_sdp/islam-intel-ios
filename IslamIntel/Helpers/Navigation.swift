//
//  UINavigationBar.swift
//  UINavigationBar
//
//  Created by Mr. Hasan on 7/31/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func transparentNavigationBar() {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
            self.isTranslucent = true
    }
}

class NavController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

class TabBarController: UITabBarController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
