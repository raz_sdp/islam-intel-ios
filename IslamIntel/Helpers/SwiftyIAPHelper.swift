//
//  SwiftyIAPHelper.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 5/10/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import StoreKit
import SwiftyStoreKit

enum RegisteredPurchase: String {
    
    case TasbihTrackerYearly = "TasbihTrackerAllAccessYearly"
}

class SwiftyIAPHelper {
    static let shared = SwiftyIAPHelper()
    
    
    //static let sessionIdSetNotification = Notification.Name("SubscriptionServiceSessionIdSetNotification")
    //static let optionsLoadedNotification = Notification.Name("SubscriptionServiceOptionsLoadedNotification")
    static let restoreSuccessfulNotification = Notification.Name("SubscriptionServiceRestoreSuccessfulNotification")
    static let purchaseSuccessfulNotification = Notification.Name("SubscriptionServiceRestoreSuccessfulNotification")
    
    //Mark: Property
    let appBundleId = "com.islamintel.IslamIntel"
    
    private init() {
        
    }
    
    //MARK:- SwiftyStoreKit
    
    //MARK: Active
    //Only for auto Renewable Subscription
    func verifyAutoRenewableSubscription(_ registeredPurchase: RegisteredPurchase, completion: @escaping (VerifyReceiptResult, VerifySubscriptionResult?) -> Void) {
        
        let productId = appBundleId + "." + registeredPurchase.rawValue
        
        //verify Subscriptions
        NetworkActivityIndicatorManager.networkOperationStarted()
        self.verifyReceipt { receiptResult in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch receiptResult {
            case .success(let receipt):
                
                let subscriptionResult = SwiftyStoreKit.verifySubscription(ofType: .autoRenewable, productId: productId, inReceipt: receipt)
                completion(receiptResult, subscriptionResult)
                
            case .error:
                completion(receiptResult, nil)
                //self.showAlert(self.alertForVerifyReceipt(receiptResult))
            }
        }
    }
    

    //MARK: Inactive
    func getInfo(_ purchase: RegisteredPurchase, completion: @escaping (RetrieveResults) -> Void) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        let productId = "TasbihTrackerYearly"
        SwiftyStoreKit.retrieveProductsInfo([productId]) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            completion(result)
            //self.showAlert(self.alertForProductRetrievalInfo(result))
        }
    }
    
    func restorePurchases() {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            for purchase in results.restoredPurchases {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                } else if purchase.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                let expireDate = purchase.transaction.transactionDate?.adjust(.year, offset: 1)
                UserDefaults.standard.set(expireDate, forKey: purchase.productId)
                
            }
            //self.showAlert(self.alertForRestorePurchases(results))
        }
    }
    
    
    
    func purchase(_ purchase: RegisteredPurchase, atomically: Bool) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
//            if let alert = self.alertForPurchaseResult(result) {
//                self.showAlert(alert)
//            }
        }
    }
    

    
    func verifyReceipt() {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            //self.showAlert(self.alertForVerifyReceipt(result))
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "402e0cc3fcb84abfa7dc0528c14492c5")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func verifyPurchase(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                
                let productId = self.appBundleId + "." + purchase.rawValue
                
                switch purchase {
                case .TasbihTrackerYearly:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt)
                    //self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
                default:
                    let purchaseResult = SwiftyStoreKit.verifyPurchase(
                        productId: productId,
                        inReceipt: receipt)
                    //self.showAlert(self.alertForVerifyPurchase(purchaseResult, productId: productId))
                }
                
            case .error: break
                //self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productIds = Set(purchases.map { self.appBundleId + "." + $0.rawValue })
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                //self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: productIds))
            case .error: break
                //self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
}
