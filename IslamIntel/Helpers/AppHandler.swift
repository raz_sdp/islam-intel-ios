//
//  AppHandler.swift
//  IslamIntel
//
//  Created by Mehedi Hasan on 19/8/18.
//  Copyright © 2018 Mehedi Hasan. All rights reserved.
//

import Foundation
import StoreKit
import Alamofire

class AppHandler {
    
    static let shared: AppHandler = AppHandler()
    
    //Mark: Property
    var helpText: HelpText!
    
    private init() {
        
    }
    
    //MARK:- All
    let AppOpenedFirstTimeKey = "AppOpenedFirstTimeKey"
    
    func updateAppWhenAppOpened(){
        let userDefault = UserDefaults.standard
        let opened = userDefault.string(forKey: AppOpenedFirstTimeKey)
        
        if opened == nil {
            //Update App For First Time
            //Tasbih
            userDefault.set(true, forKey: TasbihCounterSoundKey)
            
            userDefault.set("Opened", forKey: AppOpenedFirstTimeKey)
        }
        
        //Tasbih
        self.updateTasbihParams()
    }

    
    //MARK:- Tasbih
    func updateTasbihParams(){
        self.updateMaxLimitUnsaved()
    }
    
    var deafultSorting = true
    
    //MARK: Max Limit for Unsaved
    let MaxLimitUnsavedKey = "MaxLimitUnsavedKey"

    
    func updateMaxLimitUnsaved() {
        let userDefault = UserDefaults.standard
        let maxLimit = userDefault.integer(forKey: MaxLimitUnsavedKey)
        
        if maxLimit == 0 {
            //Default
            userDefault.set(Constants.maxTasbihCountLimit, forKey: MaxLimitUnsavedKey)
            userDefault.synchronize()
        } else {
            Constants.maxTasbihCountLimit = maxLimit
        }
    }
    
    func setMaxLimitUnsaved(value: Int) {
        Constants.maxTasbihCountLimit = value
        let userDefault = UserDefaults.standard
        userDefault.set(value, forKey: MaxLimitUnsavedKey)
        userDefault.synchronize()
    }
    
    //MARK: Sound
    let TasbihCounterSoundKey = "TasbihCounterSoundKey"
    var tasbihCounterSoundEnabled: Bool {
        get {
            return UserDefaults.standard.bool(forKey: TasbihCounterSoundKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: TasbihCounterSoundKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    //MARK:- RatingReview
    let RatingReviewDateKey = "RatingReviewDateKey"
    let RatingReviewCancelKey = "RatingReviewCancelKey"
    
    func checkRatingReview() -> Bool {
        let userDefault = UserDefaults.standard
        if let date = userDefault.object(forKey: RatingReviewDateKey) as? Date {
            let result = date.since(Date(), in: .day)
            print("checkRatingReview: \(result)")
            let waitDay = userDefault.bool(forKey: RatingReviewCancelKey) ? 7 : 3
            
            if result >= waitDay {
                return true
            } else {
                return false
            }
        } else {
            //For the first time, app open
            userDefault.set(Date(), forKey: RatingReviewDateKey)
            userDefault.synchronize()
            return false
        }
    }
    
    func checkAndAskForReview() {
        let requestReview = self.checkRatingReview()
        if requestReview {
            self.requestReview()
        }
    }
    
    func requestReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
            
        } else {
            
        }
    }
    
    func openRateApp() {
        self.openUrl("itms-apps://itunes.apple.com/app/" + Constants.appId + "?action=write-review")
        //https://itunes.apple.com/us/app/islam-intel/id1409442221?ls=1&mt=8
        //action=write-review
    }
    
    fileprivate func openUrl(_ urlString:String) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
