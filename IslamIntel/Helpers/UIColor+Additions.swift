//
//  UIColor+Additions.swift
//  UIColor+Additions
//
//  Created by Mr. Hasan on 12/28/17.
//  Copyright © 2017 Mehedi Hasan. All rights reserved.
//

import UIKit

extension UIColor {

    /*
     Color Name: Blue
     Hex: #0a8ee4
     RGB: (223,223,223)
     Swift: UIColor(red:0.04, green:0.56, blue:0.89, alpha:1.0)
    */
    
    convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: alpha
        )
    }
    
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            alpha: alpha
        )
    }
    
    // MARK: Template
    //0A8EE4
    static var templateBase: UIColor {
        return UIColor(red:0.04, green:0.56, blue:0.89, alpha:1.0)
    }
    
    static var templateBaseDark: UIColor {
        return UIColor(red:0.15, green:0.63, blue:0.50, alpha:1.0)
    }
    
    //#777777
    static var templateGray: UIColor {
        return UIColor(red:0.47, green:0.47, blue:0.47, alpha:1.0)
    }
    
    //#7c7c7c
    static var templateDarkGray: UIColor {
        return UIColor(red:0.49, green:0.49, blue:0.49, alpha:1.0)
    }
    
    static var templateBaseLightGray: UIColor {
        return UIColor(red:0.72, green:0.72, blue:0.72, alpha:1.0)
    }
    
    static var templateBaseLight: UIColor {
        return UIColor(red:0.32, green:0.89, blue:0.74, alpha:1.0)
    }
    
    
    static var templateRed: UIColor {
        return UIColor(red:0.98, green:0.36, blue:0.45, alpha:1.0)
    }
    
    //60686D
    static var templateTextFieldTextColor: UIColor {
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    

    static var templateOverallColor: UIColor {
        //0cbed9
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    static var templateYearColor: UIColor {
        //4f55e3
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    static var templateMonthColor: UIColor {
        //0a8ee4
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    static var templateWeekColor: UIColor {
        //611d75
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    static var templateDayColor: UIColor {
        //1d3475
        return UIColor(red:0.38, green:0.41, blue:0.43, alpha:1.0)
    }
    
    
    
    
    ///e40a4b
    static var templateWaning: UIColor {
        return UIColor(red:0.89, green:0.04, blue:0.29, alpha:1.0)
    }
    
    
    static var primaryText: UIColor {
        return .white
    }
    
    static var secondaryButton: UIColor {
        return UIColor(red: 245.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    }
    
    static var secondaryBorder: UIColor {
        return UIColor(red: 213.0/255.0, green: 218.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    }
    
    static var darkText: UIColor {
        return UIColor(red: 35.0/255.0, green: 37.0/255.0, blue: 38.0/255.0, alpha: 1.0)
    }
    
    static var darkLine: UIColor {
        return UIColor(red: 36.0/255.0, green: 35.0/255.0, blue: 38.0/255.0, alpha: 1.0)
    }
    
    static var secondaryShadow: UIColor {
        return UIColor(red: 213.0/255.0, green: 218.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    }
    
    // MARK: Gradient
    static var gradientStart: UIColor {
        return UIColor(red: 247.0/255.0, green: 164.0/255.0, blue: 69.0/255.0, alpha: 1.0)
    }
    
    static var gradientEnd: UIColor {
        return UIColor(red: 252.0/255.0, green: 83.0/255.0, blue: 148.0/255.0, alpha: 1.0)
    }
    
    static var offWhite: UIColor {
        return UIColor(white: 247.0/255.0, alpha: 1.0)
    }
    
    static var borderGray: UIColor {
        return UIColor(red:0.72, green:0.72, blue:0.72, alpha:1.0)
    }
    
    static var separatorGray: UIColor {
        return UIColor(white: 221.0/255.0, alpha: 1.0)
    }
    
    static var grayText: UIColor {
        return UIColor(white: 136.0/255.0, alpha: 1.0)
    }
    
    static var grayTextTint: UIColor {
        return UIColor(red: 163.0/255.0, green: 168.0/255.0, blue: 173.0/255.0, alpha: 1.0)
    }
    
    static var secondaryGrayText: UIColor {
        return UIColor(red: 101.0/255.0, green: 105.0/255.0, blue: 110.0/255.0, alpha: 1.0)
    }
    
    static var grayBackgroundTint: UIColor {
        return UIColor(red: 250.0/255.0, green: 251.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    }
    
    static var cameraGuidePositive: UIColor {
        return UIColor(red: 72.0/255.0, green: 240.0/255.0, blue: 184.0/255.0, alpha: 1.0)
    }
    
    static var cameraGuideNegative: UIColor {
        return UIColor(red: 240.0/255.0, green: 74.0/255.0, blue: 93.0/255.0, alpha: 1.0)
    }
    
    static var purple: UIColor {
        return UIColor(red: 209.0/255.0, green: 125.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    
    static var darkPurple: UIColor {
        return UIColor(red: 127.0/255.0, green: 83.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    }
    
    static var pink: UIColor {
        return UIColor(red: 252.0/255.0, green: 83.0/255.0, blue: 148.0/255.0, alpha: 1.0)
    }
    
    static var blue: UIColor {
        return UIColor(red: 76.0/255.0, green: 152.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    }
    
    static var whiteTint: UIColor {
        return UIColor(red: 245.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    }
    
    static var transparentWhite: UIColor {
        return UIColor(white: 1.0, alpha: 0.3)
    }
    
    static var transparentBlack: UIColor {
        return UIColor(white: 0.0, alpha: 0.3)
    }
    
    static var blueGradientStart: UIColor {
        return UIColor(red: 99.0/255.0, green: 188.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    static var blueGradientEnd: UIColor {
        return UIColor(red: 56.0/255.0, green: 141.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    }
    
    static var txListGreen: UIColor {
        return UIColor(red: 0.0, green: 169.0/255.0, blue: 157.0/255.0, alpha: 1.0)
    }
}
